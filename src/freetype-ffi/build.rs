extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rustc-link-lib=freetype");

    let regex = "(?:BDF|CID|FT|PS|T1|TT).*";

    let bindings = bindgen::Builder::default()
        .header("./stub.h")
        .clang_arg("-I./vendor/freetype2/include")
        .whitelist_function(regex)
        .whitelist_type(regex)
        .whitelist_var(regex)
        .default_enum_style(bindgen::EnumVariation::Consts)
        .prepend_enum_name(false)
        .layout_tests(false)
        .generate_comments(false)
        .impl_debug(true)
        .derive_default(true)
        .rust_target(bindgen::RustTarget::Nightly)
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
