# freetype-ffi

Ad-hoc generated bindings to FreeType 2. This crate consists solely of
the output of `bindgen`, as FreeType is a large, complex API not built
with code generation in mind. Creating a more bespoke set of bindings
is unlikely to happen as that would involve making non-trivial design
decisions that require some level of expertise with respect to the
original API.
