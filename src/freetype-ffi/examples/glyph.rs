//! This example reads a font file, rasterizes a glyph, and displays it
//! as ASCII art.
extern crate freetype_ffi as ft;

use std::os::raw::*;
use std::ptr;

macro_rules! c_str {
    ($str:expr) => {
        concat!($str, "\0") as *const str as *const ::std::os::raw::c_char
    }
}

const FONT_FILE: *const c_char =
    c_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/data/DejaVuSans.ttf"));

fn main() {
    unsafe { unsafe_main() }
}

fn pixel_to_char(value: u8) -> char {
    match (4 * (value as u32) + 127) / 255 {
        0 => ' ',
        1 => '░',
        2 => '▒',
        3 => '▓',
        4 => '█',
        _ => unreachable!(),
    }
}

fn render_glyph(data: &[u8], rows: usize, width: usize) {
    assert!(data.len() >= rows * width);
    for i in 0..rows {
        let row = &data[width * i..width * (i + 1)];
        for &p in row.iter() {
            print!("{}", pixel_to_char(p));
        }
        println!();
    }
}

unsafe fn unsafe_main() {
    let mut lib: ft::FT_Library = ptr::null_mut();
    ft::check!(ft::FT_Init_FreeType(&mut lib as _)).unwrap();

    let mut face: ft::FT_Face = ptr::null_mut();
    ft::check!
        (ft::FT_New_Face(lib, FONT_FILE as *const _ as _, 0, &mut face as _))
        .unwrap();

    ft::check!(ft::FT_Set_Pixel_Sizes(face, 0, 16)).unwrap();
    ft::check!(ft::FT_Load_Char(face, 'S' as _, ft::FT_LOAD_RENDER as _))
        .unwrap();

    let bm = (*(*face).glyph).bitmap;
    let pitch = bm.pitch as u32;
    assert_eq!(bm.pixel_mode, ft::FT_PIXEL_MODE_GRAY as _);
    assert_eq!(bm.width, pitch);
    let data: &'static [u8] = std::slice::from_raw_parts
        (bm.buffer as _, (bm.rows * pitch) as _);
    render_glyph(data, bm.rows as _, bm.width as _);

    ft::FT_Done_Face(face);
    ft::FT_Done_FreeType(lib);
}
