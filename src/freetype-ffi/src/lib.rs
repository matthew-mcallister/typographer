#![allow(clippy::unreadable_literal)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#[macro_export]
macro_rules! check {
    ($res:expr) => { if $res != 0 { Err($res) } else { Ok(()) } }
}

// Missing definitions below

macro_rules! load_targets {
    ($($name:ident = $mem:ident;)*) => {
        $(pub const $name: i32 = (($mem & 0xF) << 16) as _;)*
    }
}

load_targets! {
    FT_LOAD_TARGET_NORMAL = FT_RENDER_MODE_NORMAL;
    FT_LOAD_TARGET_LIGHT  = FT_RENDER_MODE_LIGHT;
    FT_LOAD_TARGET_MONO   = FT_RENDER_MODE_MONO;
    FT_LOAD_TARGET_LCD    = FT_RENDER_MODE_LCD;
    FT_LOAD_TARGET_LCD_V  = FT_RENDER_MODE_LCD_V;
}
