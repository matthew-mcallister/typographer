//! This module implements the `ScriptIter` type, which finds breaks up
//! text into homogenous runs based on script for use by graphical
//! applications.
use crate::*;
use crate::bracket::*;

/// A stretch of text written in a uniform script. This is returned by
/// iterating over the `ScriptIter` type.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Run {
    /// Length in bytes.
    pub len: usize,
    pub script: Script,
}

#[derive(Clone, Copy, Debug)]
struct StackEntry {
    // Counter to adjust for the fact that `pos` is not absolute
    gen: u32,
    pos: usize,
    ch: char,
    script: Script,
}

#[derive(Clone, Debug)]
struct CharInfo {
    pos: usize,
    ch: char,
    sc: Script,
    scx: Option<&'static [Script]>,
    // N.B. contains bogus info if none exists for the char.
    br: Option<BracketInfo>,
}

const fn dummy_char_info() -> CharInfo {
    CharInfo {
        pos: 0,
        ch: '\0',
        sc: Script::Unknown,
        scx: None,
        br: None,
    }
}

/// An iterator over the same-script runs in a piece of text.
///
/// # Examples
///
/// ```
/// use unic_script::{*, run::*};
/// let input = "Hello, 世界!";
/// let runs: Vec<_> = ScriptIter::new(input).collect();
/// assert_eq!(&runs[..], &[
///     Run { len: 7, script: Script::Latin },
///     Run { len: 7, script: Script::Han },
/// ]);
/// ```
#[derive(Clone, Debug)]
pub struct ScriptIter<'str> {
    target: &'str str,
    chars: std::str::Chars<'str>,
    gen: u32,
    stack: Vec<StackEntry>,
    run_sc: Script,
    cur_ch: CharInfo,
    // Conceptually, this is a stack of length 1
    runs: Option<Run>,
}

impl<'str> ScriptIter<'str> {
    /// Initializes a new iterator.
    pub fn new(source: &'str str) -> Self {
        let mut iter = ScriptIter {
            target: source,
            chars: source.chars(),
            gen: 0,
            stack: Vec::new(),
            run_sc: Script::Unknown,
            cur_ch: dummy_char_info(),
            runs: None,
        };
        // Get the first char
        scan_advance(&mut iter);
        iter
    }
}

impl<'str> Iterator for ScriptIter<'str> {
    type Item = Run;
    fn next(&mut self) -> Option<Self::Item> { next_run(self) }
}

impl<'str> std::iter::FusedIterator for ScriptIter<'str> {}

macro_rules! iter_offset {
    ($src:expr, $chars:expr) => { $src.len() - $chars.as_str().len() }
}

fn ambig_script(sc: Script) -> bool {
    sc == Script::Common || sc == Script::Inherited
}

fn compat_script_ext(run_sc: Script, sc: Script) -> bool {
    sc == run_sc || ambig_script(sc)
}

// Advances the script iterator forward one character. Updates the
// bracket stack.
fn advance_inner(
    iter: &mut ScriptIter<'_>,
    get_exts: bool,
    raw_sc: bool,
) -> Option<()> {
    let pos = iter_offset!(iter.target, iter.chars);
    let ch = iter.chars.next()?;

    let mut sc = Script::from_char(ch);

    let br;
    if sc == Script::Common {
        br = bracket_info(ch);
        if let Some(info) = br {
            if info.side.is_open() {
                // Push an entry onto the stack.
                iter.stack.push(StackEntry {
                    gen: iter.gen,
                    pos,
                    ch: info.opp,
                    script: iter.run_sc,
                });
            } else {
                // Pop the matching entry off the stack as well as
                // any unmatched entries above it.
                while let Some(entry) = iter.stack.pop() {
                    if ch == entry.ch {
                        if !raw_sc { sc = entry.script; }
                        break;
                    }
                }
            }
        }
    } else { br = None; }

    let scx = if get_exts && ambig_script(sc) {
        Some(Script::raw_exts_from_char(ch)
            .unwrap_or_else(|| sc.as_singleton()))
    } else { None };

    iter.cur_ch = CharInfo { pos, ch, sc, scx, br };

    Some(())
}

// Used when looking for the end of a run
fn advance(iter: &mut ScriptIter<'_>) -> Option<()> {
    advance_inner(iter, true, false)
}

// Used when looking for the start of a run
fn scan_advance(iter: &mut ScriptIter<'_>) -> Option<()> {
    advance_inner(iter, false, true)
}

// Scans the text until a char with definite script is found. Returns
// `None` upon reaching the end of input.
//
// `Unknown` is perhaps surprisingly treated as definite, as any char
// so designated *could* belong to a specific script, just not one
// currently recognized by Unicode.
fn scan_to_script(iter: &mut ScriptIter<'_>) -> Option<Script> {
    if iter.target.is_empty() { return None; }
    loop {
        if !ambig_script(iter.cur_ch.sc) {
            return Some(iter.cur_ch.sc);
        }
        scan_advance(iter)?;
    }
}

// Finds the exact start point of a run when the script is known.
fn find_run_start(iter: &ScriptIter<'_>) -> usize {
    let mut start = 0;
    let mut can_inherit = false;
    let slice = &iter.target[..iter.cur_ch.pos];
    let mut chars = slice.chars();
    while let Some(ch) = chars.next() {
        let sc = Script::from_char(ch);
        debug_assert!(
            // Clearly we've iterated too far
            sc == Script::Common || sc == Script::Inherited,
            "run_sc = {:?}, sc = {:?}", iter.run_sc, sc,
        );

        // We can totally ignore brackets at this point.
        if sc == Script::Inherited && !can_inherit {
            start = iter_offset!(slice, chars);
        } else {
            let scx = Script::raw_exts_from_char(ch)
                .unwrap_or_else(|| sc.as_singleton());
            if scx.iter().any(|&sc_| compat_script_ext(iter.run_sc, sc_)) {
                can_inherit = true;
            } else {
                start = iter_offset!(slice, chars);
                can_inherit = false;
            }
        }
    }

    start
}

fn scan_to_end(iter: &mut ScriptIter<'_>) -> usize {
    loop {
        if advance(iter).is_none() { return iter.target.len(); }
        if iter.cur_ch.sc != iter.run_sc && !(
            ambig_script(iter.cur_ch.sc)
            && iter.cur_ch.scx.unwrap().iter()
                .any(|&sc| compat_script_ext(iter.run_sc, sc))
        ) {
            return iter.cur_ch.pos;
        }
    }
}

fn next_run(iter: &mut ScriptIter<'_>) -> Option<Run> {
    // N.B.: This algorithm is greatly modified from Pango's and
    // incorporates the advice to implementors found in UAX #24.
    //
    // There remain several issues that can only be resolved by
    // applying language-dependent analysis, e.g. quotation marks are
    // not properly matched and end sentence punctuation may not the
    // the sentence as a whole.
    if let Some(run) = iter.runs.take() { return Some(run); }

    // Scan forward until we find a script to use
    match scan_to_script(iter) {
        Some(sc) => iter.run_sc = sc,
        None => {
            // Text is exhausted
            let len = iter.target.len();
            iter.target = "";
            return if len > 0 {
                Some(Run { len, script: Script::Unknown })
            } else { None };
        },
    };

    let start = find_run_start(iter);

    // Retroactively apply script information to the bracket stack.
    for entry in iter.stack.iter_mut().rev() {
        if entry.gen < iter.gen || entry.pos < start { break; }
        entry.script = iter.run_sc;
    }

    let end = scan_to_end(iter);

    let run = Run { len: end - start, script: iter.run_sc };

    // Reset for next iteration
    iter.run_sc = Script::Unknown;
    iter.target = &iter.target[end..];
    iter.cur_ch.pos = 0;
    iter.gen += 1;

    if start > 0 {
        // Any chars prior to the run start become a run of unknowns.
        iter.runs = Some(run);
        Some(Run { len: start, script: Script::Unknown })
    } else { Some(run) }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_script_iter() {
        const PAIRS: &'static [(&'static str, &'static [Run])] = &[
            ("Hello, world!", &[Run { len: 13, script: Script::Latin }]),
            (r"\.(o.o)./", &[Run { len: 9, script: Script::Latin }]),
            ("[a]α", &[
                Run { len: 3, script: Script::Latin },
                Run { len: 2, script: Script::Greek },
            ]),
            ("aー", &[
                Run { len: 1, script: Script::Latin },
                Run { len: 3, script: Script::Unknown },
            ]),
            ("ー()a", &[
                Run { len: 3, script: Script::Unknown },
                Run { len: 3, script: Script::Latin },
            ]),
            ("()a", &[Run { len: 3, script: Script::Latin }]),
            ("a({[⟨א⟩]})ナ", &[
                Run { len: 7, script: Script::Latin },
                Run { len: 2, script: Script::Hebrew },
                Run { len: 6, script: Script::Latin },
                Run { len: 3, script: Script::Katakana },
            ]),
            ("\u{0300}'Ranso\u{0301}m'", &[
                Run { len: 2, script: Script::Unknown },
                Run { len: 10, script: Script::Latin },
            ]),
            ("α(}c)", &[
                Run { len: 4, script: Script::Greek },
                Run { len: 2, script: Script::Latin },
            ]),
            ("α(b}c)", &[
                Run { len: 3, script: Script::Greek },
                Run { len: 4, script: Script::Latin },
            ]),
        ];
        for (input, expected) in PAIRS.iter() {
            let runs: Vec<_> = ScriptIter::new(input).collect();
            assert_eq!(&runs, expected, "\n input: {:?}", input);
        }
    }
}
