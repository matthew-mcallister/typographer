macro_rules! impl_non_ascii_brackets {
    ($(($fst:expr, $snd:expr, $type:ident))*) => {
        const NON_ASCII_BRACKETS: &'static [(char, BracketInfo)] = &[
            $(($fst, BracketInfo { opp: $snd, side: Side::$type }),)*
        ];
    }
}

include!(concat!(env!("OUT_DIR"), "/impl_brackets.rs"));

// The lateralization of a bracket.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
crate enum Side {
    Open,
    Close,
}

impl Side {
    #[allow(dead_code)]
    crate fn opposite(self) -> Self {
        match self {
            Side::Open => Side::Close,
            Side::Close => Side::Open,
        }
    }

    crate fn is_open(self) -> bool { self == Side::Open }

    #[allow(dead_code)]
    crate fn is_close(self) -> bool { self == Side::Close }
}

// Gives information about a bracket character.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
crate struct BracketInfo {
    crate side: Side,
    crate opp: char,
}

// Returns information about a paired bracket character.
//
// TODO: This should only operate on characters with a script of
// `Common`!
crate fn bracket_info(ch: char) -> Option<BracketInfo> {
    if ch.is_ascii() {
        Some(match ch {
            '(' => BracketInfo { side: Side::Open, opp: ')' },
            ')' => BracketInfo { side: Side::Close, opp: '('},
            '[' => BracketInfo { side: Side::Open, opp: ']'},
            ']' => BracketInfo { side: Side::Close, opp: '['},
            '{' => BracketInfo { side: Side::Open, opp: '}'},
            '}' => BracketInfo { side: Side::Close, opp: '{'},
            _ => return None,
        })
    } else {
        let i = NON_ASCII_BRACKETS.binary_search_by_key(&ch, |(c, _)| *c).ok()?;
        Some(NON_ASCII_BRACKETS[i].1)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bracket_info() {
        let pairs = &[
            ('\u{0F3A}', None),
            ('\u{207D}',
                Some(BracketInfo { side: Side::Open, opp: '\u{207E}' })),
        ];
        for (ch, info) in pairs.iter().cloned() {
            assert_eq!(bracket_info(ch), info);
        }
    }

    #[test]
    fn test_bracket_symmetry() {
        for (ch, bracket) in NON_ASCII_BRACKETS.iter().cloned() {
            let opp = bracket_info(bracket.opp).unwrap();
            assert_eq!(opp.side, bracket.side.opposite());
            assert_eq!(opp.opp, ch);
        }
    }
}
