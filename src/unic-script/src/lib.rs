//! This crate provides code for working with the Unicode `Script` and
//! `Script_Extensions` properties. The `Script` type enumerates the
//! possible script values defined by Unicode and provides functions for
//! retrieving the script(s) assigned to a character.
//!
//! The `run` submodule implements an algorithm to break up text into
//! runs of characters of the same script.
//!
//! See [Unicode Standard Annex #24][tr24] for details on the assignment
//! and semantics of script codes.
//!
//! The current implementation is up-to-date with Unicode version 11.0.
//!
//! [tr24]: https://www.unicode.org/reports/tr24/
#![feature(crate_visibility_modifier)]
#![feature(non_exhaustive)]

use std::ops::Range;

mod bracket;

pub mod run;

macro_rules! impl_script_enum {
    ($(($index:expr, $name:ident, $short:ident))*) => {
        /// Enumerates the values of the Unicode `Script` property that
        /// may be assigned to a character.
        #[non_exhaustive]
        #[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
        pub enum Script {
            $($name = $index,)*
        }

        impl Script {
            /// Returns the Unicode-designated 4-letter short name of
            /// the script. The short name is generally compatible with
            /// the ISO 15924 code assigments for the same script,
            /// though ISO 15924 may assign multiple codes to a script
            /// that only has one code under Unicode.
            ///
            /// # Examples
            ///
            /// ```
            /// use unic_script::*;
            /// assert_eq!(Script::Latin.short_name(), "Latn");
            /// ```
            pub fn short_name(self) -> &'static str {
                match self {$(
                    Script::$name => stringify!($short),
                )*}
            }

            /// Returns the script's short name as a sequence of ASCII
            /// bytes.
            ///
            /// # Examples
            ///
            /// ```
            /// use unic_script::*;
            /// assert_eq!(Script::Latin.short_name_bytes(), *b"Latn");
            /// ```
            #[inline]
            pub fn short_name_bytes(self) -> [u8; 4] {
                // Every name is 4 bytes long, so this cast is safe
                unsafe { *(self.short_name() as *const str as *const [u8; 4]) }
            }

            // This is a hack to return a static lifetime,
            // single-element slice from the `exts_from_char` function.
            // TODO: If `raw_exts_from_char` is public, this probably
            // should be as well.
            #[inline(always)]
            crate fn as_singleton(self) -> &'static [Script] {
                const SINGLETONS: &'static [Script] = &[$(Script::$name,)*];
                let idx = self as usize;
                &SINGLETONS[idx..idx + 1]
            }
        }

        // Used internally in the `Script_Extensions` table
        #[allow(dead_code)]
        #[allow(non_upper_case_globals)]
        mod short_names {
            use super::Script;
            $(crate const $short: Script = Script::$name;)*
        }
    }
}

include!(concat!(env!("OUT_DIR"), "/impl_script_enum.rs"));

macro_rules! impl_script_lookup_table {
    ($($script:ident,)*) => {
        const SCRIPT_LOOKUP_TABLE: &'static [Script] =
            &[$(Script::$script,)*];
    }
}

include!(concat!(env!("OUT_DIR"), "/impl_script_lookup_table.rs"));

macro_rules! impl_script_search_table {
    ($(($range:expr, $script:ident))*) => {
        const SCRIPT_SEARCH_TABLE: &'static [(Range<u32>, Script)] =
            &[$(($range, Script::$script),)*];
    }
}

include!(concat!(env!("OUT_DIR"), "/impl_script_search_table.rs"));

macro_rules! impl_script_ext_table {
    ($(($range:expr, [$($script:ident,)*]))*) => {
        const SCRIPT_EXT_TABLE: &'static [(Range<u32>, &'static [Script])] =
            &[$(($range, &[$(short_names::$script,)*]),)*];
    }
}

include!(concat!(env!("OUT_DIR"), "/impl_script_ext_table.rs"));

#[inline(always)]
fn range_cmp<T: Ord>(c: T, range: &Range<T>) ->
    std::cmp::Ordering
{
    if range.end <= c { std::cmp::Ordering::Less }
    else if c < range.start { std::cmp::Ordering::Greater }
    else { std::cmp::Ordering::Equal }
}

impl Script {
    /// Returns the script assigned to a Unicode character.
    ///
    /// # Examples
    ///
    /// ```
    /// use unic_script::*;
    /// assert_eq!(Script::from_char('a'), Script::Latin);
    /// assert_eq!(Script::from_char('😂'), Script::Common);
    /// assert_eq!(Script::from_char('א'), Script::Hebrew);
    /// // Private use codepoint
    /// assert_eq!(Script::from_char('\u{E000}'), Script::Unknown);
    /// ```
    pub fn from_char(ch: char) -> Self {
        let c = ch as u32;
        if (c as usize) < SCRIPT_LOOKUP_TABLE.len() {
            SCRIPT_LOOKUP_TABLE[c as usize]
        } else {
            SCRIPT_SEARCH_TABLE
                .binary_search_by(|(range, _)| range_cmp(c, range))
                .map(|i| SCRIPT_SEARCH_TABLE[i].1)
                .unwrap_or(Script::Unknown)
        }
    }

    /// Returns extended script information assigned to a Unicode
    /// character.
    ///
    /// # Examples
    ///
    /// ```
    /// use unic_script::*;
    /// assert_eq!(Script::from_char_ext('a'),
    ///     (Script::Latin, &[Script::Latin][..]));
    /// assert_eq!(Script::from_char_ext('😂'),
    ///     (Script::Common, &[Script::Common][..]));
    /// assert_eq!(Script::from_char_ext('ー'),
    ///     (Script::Common, &[Script::Hiragana, Script::Katakana][..]));
    /// ```
    pub fn from_char_ext(ch: char) -> (Self, &'static [Self]) {
        let sc = Script::from_char(ch);
        let scx = if sc == Script::Common || sc == Script::Inherited {
            Script::raw_exts_from_char(ch).unwrap_or_else(|| sc.as_singleton())
        } else { sc.as_singleton() };
        (sc, scx)
    }

    /// Fetches explicit extended script information and returns the
    /// result directly. A character with no such information has an
    /// implicitly defined script set according to the standard.
    ///
    /// This information is only applicable to characters assigned the
    /// script codes `Common` or `Inherited`.
    ///
    /// # Examples
    ///
    /// ```
    /// use unic_script::*;
    /// assert_eq!(Script::raw_exts_from_char('a'), None);
    /// assert_eq!(
    ///     Script::raw_exts_from_char('ー'),
    ///     Some(&[Script::Hiragana, Script::Katakana][..]),
    /// );
    /// ```
    pub fn raw_exts_from_char(ch: char) -> Option<&'static [Self]> {
        if ch.is_ascii() { return None; }
        let res = SCRIPT_EXT_TABLE
            .binary_search_by(|(range, _)| range_cmp(ch as u32, range));
        if let Ok(i) = res { Some(SCRIPT_EXT_TABLE[i].1) }
        else { None }
    }
}

// Sanity tests
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_short_name() {
        const PAIRS: &'static [(Script, &'static str)] = &[
            (Script::Latin, "Latn"),
            (Script::Han, "Hani"),
            (Script::Inherited, "Zinh"),
            (Script::Common, "Zyyy"),
        ];
        for (sc, name) in PAIRS.iter().cloned() {
            assert_eq!(sc.short_name(), name);
            assert_eq!(&sc.short_name_bytes(), name.as_bytes());
        }
    }

    #[test]
    fn test_from_char() {
        const PAIRS: &'static [(char, Script)] = &[
            ('a', Script::Latin),
            (' ', Script::Common),
            ('\0', Script::Common),
            ('\u{00A1}', Script::Common),
            ('\u{00C0}', Script::Latin),
            ('\u{0370}', Script::Greek),
            ('\u{2000}', Script::Common),
            ('\u{2000}', Script::Common),
            ('\u{2E80}', Script::Han),
            ('\u{2E99}', Script::Han),
            ('\u{2E9A}', Script::Unknown),
            ('\u{E100}', Script::Unknown),
        ];
        for (c, sc) in PAIRS.iter().cloned() {
            assert_eq!(Script::from_char(c), sc, "\n  char: U+{:x}", c as u32);
        }
    }

    #[test]
    fn test_raw_exts_from_char() {
        const PAIRS: &'static [(char, Option<&'static [Script]>)] = &[
            (' ', None),
            ('\u{1CD1}', Some(&[Script::Devanagari])),
            ('\u{1BCA1}', Some(&[Script::Duployan])),
            ('\u{0670}', Some(&[Script::Arabic, Script::Syriac])),
            ('\u{096F}', Some(&[
                Script::Devanagari, Script::Dogra, Script::Kaithi,
                Script::Mahajani,
            ])),
        ];
        for (c, scrs) in PAIRS.iter().cloned() {
            assert_eq!(
                Script::raw_exts_from_char(c), scrs,
                "\n  char: U+{:x}", c as u32,
            );
        }
    }

    #[test]
    fn test_from_char_ext() {
        const PAIRS: &'static [(char, (Script, &'static [Script]))] = &[
            (' ', (Script::Common, &[Script::Common])),
            ('\u{1CD1}', (Script::Inherited, &[Script::Devanagari])),
            ('\u{1BCA1}', (Script::Common, &[Script::Duployan])),
        ];
        for (c, srcs) in PAIRS.iter().cloned() {
            assert_eq!(
                Script::from_char_ext(c), srcs,
                "\n  char: U+{:x}", c as u32,
            );
        }
    }
}
