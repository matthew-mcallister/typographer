# unic-script

This crate computes the Unicode® `Script` and `Script_Extensions`
properties as described in Unicode Standard Annex #24. It provides
tables for getting short names, the latter of which are compatible with
ISO 15924 4-letter codes. It also implements an algorithm for
determining runs of text in the same script for use by graphical
applications.
