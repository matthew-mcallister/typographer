#![feature(vec_resize_default)]

use freetype_ffi as ft;
use harfbuzz_ffi as hb;
use lodepng as png;

use std::io::Write;
use std::ptr;

const FONT: &'static [u8] = include_bytes!(
    concat!(env!("CARGO_MANIFEST_DIR"), "/data/DejaVuSans.ttf")
);
const TEXT: &'static str = "Hello World!";
const FONT_PIXELS: u32 = 16;

const IMAGE_DIMS: [usize; 2] = [128, 32];
const START_POS: [i32; 2] = [16, 24];

const OUTPUT_PATH: &'static str =
    concat!(env!("CARGO_MANIFEST_DIR"), "/output/hello.png");

#[derive(Clone, Debug)]
struct Image<P> {
    buffer: Vec<P>,
    width: usize,
    height: usize,
}

impl<P: Default> Image<P> {
    fn new(dims: [usize; 2]) -> Self {
        let mut buffer = Vec::new();
        buffer.resize_default(dims[0] * dims[1]);
        let [width, height] = dims;
        Image { buffer, width, height }
    }
}

type GlyphInst<'a> = (&'a hb::GlyphInfo, &'a hb::GlyphPosition);

unsafe fn buf_glyphs(buf: *mut hb::Buffer) ->
    impl Iterator<Item = GlyphInst<'static>> + 'static
{
    let mut glyph_count: u32 = 0;
    let glyph_info = hb::buffer_get_glyph_infos(buf, &mut glyph_count as _);
    let glyph_info = std::slice::from_raw_parts(glyph_info, glyph_count as _);
    let glyph_pos = hb::buffer_get_glyph_positions(buf, ptr::null_mut());
    let glyph_pos = std::slice::from_raw_parts(glyph_pos, glyph_count as _);
    Iterator::zip(glyph_info.iter(), glyph_pos.iter())
}

const fn round_f26dot6(fixed: i32) -> i32 {
    (fixed + 32) / 64
}

unsafe fn draw_glyph(
    face: ft::FT_Face,
    glyph_index: u32,
    // Lower-left corner pos (since everything is relative to baseline)
    pos: [i32; 2],
    dst: &mut Image<u8>,
) {
    ft::check!(ft::FT_Load_Glyph(face, glyph_index, ft::FT_LOAD_RENDER as _))
        .unwrap();
    let src = &(*(*face).glyph).bitmap;
    assert_eq!(src.pixel_mode, ft::FT_PIXEL_MODE_GRAY as _);

    // Convert to upper-left corner
    let pos = [pos[0], pos[1] - src.rows as i32];

    // Clip
    let src_start_col = 0.max(-pos[0]) as i32;
    let src_start_row = 0.max(-pos[1]) as i32;
    let dst_start_col = 0.max(pos[0]) as i32;
    let dst_start_row = 0.max(pos[1]) as i32;

    let num_cols
        = (dst.width as i32 - dst_start_col).min(src.width as _)
        - src_start_col;
    let num_rows
        = (dst.height as i32 - dst_start_row).min(src.rows as _)
        - src_start_row;

    if num_cols < 0 || num_cols > src.width as _
        || num_rows < 0 || num_rows > src.rows as _
        { return; }

    // Blit
    for row in 0..num_rows {
        let src_offset =
            (src_start_row + row) * src.pitch as i32 + src_start_col;
        let dst_offset =
            (dst_start_row + row) * dst.width as i32 + dst_start_col;
        ptr::copy_nonoverlapping(
            src.buffer.offset(src_offset as _),
            dst.buffer.as_mut_ptr().offset(dst_offset as _),
            num_cols as _,
        );
    }
}

fn main() {
    unsafe { unsafe_main() }
}

unsafe fn unsafe_main() {
    // Load font
    let mut ft_lib: ft::FT_Library = ptr::null_mut();
    ft::check!(ft::FT_Init_FreeType(&mut ft_lib as _)).unwrap();

    let mut ft_face: ft::FT_Face = ptr::null_mut();
    ft::check!(ft::FT_New_Memory_Face(
        ft_lib,
        FONT as *const _ as _,
        FONT.len() as _,
        0,
        &mut ft_face as _,
    )).unwrap();
    ft::check!(ft::FT_Set_Pixel_Sizes(ft_face, 0, FONT_PIXELS as _)).unwrap();

    let font = hb::ft::font_create(ft_face, None);

    // Create text buffer
    let buf = hb::buffer_create();
    hb::buffer_add_utf8
        (buf, TEXT as *const _ as _, TEXT.len() as _, 0, TEXT.len() as _);

    hb::buffer_set_direction(buf, hb::Direction::LTR);
    hb::buffer_set_script(buf, hb::Script::LATIN);
    hb::buffer_set_language
        (buf, hb::language_from_string(&b"en" as *const _ as _, 2));
    hb::buffer_set_cluster_level
        (buf, hb::BufferClusterLevel::MONOTONE_CHARACTERS);

    // Draw text buffer to image
    hb::shape(font, buf, ptr::null(), 0);

    let mut image = Image::new(IMAGE_DIMS);
    let [mut pen_x, mut pen_y] = START_POS;
    for (info, pos) in buf_glyphs(buf) {
        let glyph_pos = [
            pen_x + round_f26dot6(pos.x_offset),
            pen_y + round_f26dot6(pos.y_offset),
        ];
        let glyph_index = info.codepoint;
        draw_glyph(ft_face, glyph_index, glyph_pos, &mut image);
        pen_x += round_f26dot6(pos.x_advance);
        pen_y += round_f26dot6(pos.y_advance);
    }

    // Write image to file
    let image = png::encode_memory(
        &image.buffer,
        image.width,
        image.height,
        png::ColorType::GREY,
        8,
    ).unwrap();
    let mut file = std::fs::OpenOptions::new()
        .write(true).truncate(true).create(true).open(OUTPUT_PATH).unwrap();
    file.write_all(&image).unwrap();

    // Clean up
    hb::buffer_destroy(buf);
    hb::font_destroy(font);

    ft::FT_Done_Face(ft_face);
    ft::FT_Done_FreeType(ft_lib);
}
