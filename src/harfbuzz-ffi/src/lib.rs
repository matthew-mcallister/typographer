#![feature(extern_types)]

use std::ops::*;
use std::os::raw::*;

pub type Bool = c_int;
pub type Codepoint = u32;
pub type Position = i32;
pub type Mask = u32;
pub type Tag = u32;
pub type Language = *const LanguageImpl;
pub type Color = u32;

macro_rules! impl_enum {
    ($name:ident; $($mem:ident = $val:expr;)*) => {
        impl_enum!($name(u32); $($mem = $val;)*);
    };
    ($name:ident($type:ty); $($mem:ident = $val:expr;)*) => {
        #[repr(C)]
        #[derive(
            Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd,
        )]
        pub struct $name(pub $type);
        impl $name { $(pub const $mem: $name = $name($val);)* }
        impl From<$type> for $name {
            fn from(val: $type) -> Self { $name(val) }
        }
        impl From<$name> for $type {
            fn from(val: $name) -> Self { val.0 }
        }
    };
}

impl_enum! {
    Direction;
    INVALID = 0;
    LTR = 4;
    RTL = 5;
    TTB = 6;
    BTT = 7;
}
impl_enum! {
    Script;
    COMMON = 1517910393;
    INHERITED = 1516858984;
    UNKNOWN = 1517976186;
    ARABIC = 1098015074;
    ARMENIAN = 1098018158;
    BENGALI = 1113943655;
    CYRILLIC = 1132032620;
    DEVANAGARI = 1147500129;
    GEORGIAN = 1197830002;
    GREEK = 1198679403;
    GUJARATI = 1198877298;
    GURMUKHI = 1198879349;
    HANGUL = 1214344807;
    HAN = 1214344809;
    HEBREW = 1214603890;
    HIRAGANA = 1214870113;
    KANNADA = 1265525857;
    KATAKANA = 1264676449;
    LAO = 1281453935;
    LATIN = 1281455214;
    MALAYALAM = 1298954605;
    ORIYA = 1332902241;
    TAMIL = 1415671148;
    TELUGU = 1415933045;
    THAI = 1416126825;
    TIBETAN = 1416192628;
    BOPOMOFO = 1114599535;
    BRAILLE = 1114792297;
    CANADIAN_SYLLABICS = 1130458739;
    CHEROKEE = 1130915186;
    ETHIOPIC = 1165256809;
    KHMER = 1265134962;
    MONGOLIAN = 1299148391;
    MYANMAR = 1299803506;
    OGHAM = 1332175213;
    RUNIC = 1383427698;
    SINHALA = 1399418472;
    SYRIAC = 1400468067;
    THAANA = 1416126817;
    YI = 1500080489;
    DESERET = 1148416628;
    GOTHIC = 1198486632;
    OLD_ITALIC = 1232363884;
    BUHID = 1114990692;
    HANUNOO = 1214344815;
    TAGALOG = 1416064103;
    TAGBANWA = 1415669602;
    CYPRIOT = 1131442804;
    LIMBU = 1281977698;
    LINEAR_B = 1281977954;
    OSMANYA = 1332964705;
    SHAVIAN = 1399349623;
    TAI_LE = 1415670885;
    UGARITIC = 1432838514;
    BUGINESE = 1114990441;
    COPTIC = 1131376756;
    GLAGOLITIC = 1198285159;
    KHAROSHTHI = 1265131890;
    NEW_TAI_LUE = 1415670901;
    OLD_PERSIAN = 1483761007;
    SYLOTI_NAGRI = 1400466543;
    TIFINAGH = 1415999079;
    BALINESE = 1113681001;
    CUNEIFORM = 1483961720;
    NKO = 1315663727;
    PHAGS_PA = 1349017959;
    PHOENICIAN = 1349021304;
    CARIAN = 1130459753;
    CHAM = 1130914157;
    KAYAH_LI = 1264675945;
    LEPCHA = 1281716323;
    LYCIAN = 1283023721;
    LYDIAN = 1283023977;
    OL_CHIKI = 1332503403;
    REJANG = 1382706791;
    SAURASHTRA = 1398895986;
    SUNDANESE = 1400204900;
    VAI = 1449224553;
    AVESTAN = 1098281844;
    BAMUM = 1113681269;
    EGYPTIAN_HIEROGLYPHS = 1164409200;
    IMPERIAL_ARAMAIC = 1098018153;
    INSCRIPTIONAL_PAHLAVI = 1349020777;
    INSCRIPTIONAL_PARTHIAN = 1349678185;
    JAVANESE = 1247901281;
    KAITHI = 1265920105;
    LISU = 1281979253;
    MEETEI_MAYEK = 1299473769;
    OLD_SOUTH_ARABIAN = 1398895202;
    OLD_TURKIC = 1332898664;
    SAMARITAN = 1398893938;
    TAI_THAM = 1281453665;
    TAI_VIET = 1415673460;
    BATAK = 1113683051;
    BRAHMI = 1114792296;
    MANDAIC = 1298230884;
    CHAKMA = 1130457965;
    MEROITIC_CURSIVE = 1298494051;
    MEROITIC_HIEROGLYPHS = 1298494063;
    MIAO = 1349284452;
    SHARADA = 1399353956;
    SORA_SOMPENG = 1399812705;
    TAKRI = 1415670642;
    BASSA_VAH = 1113682803;
    CAUCASIAN_ALBANIAN = 1097295970;
    DUPLOYAN = 1148547180;
    ELBASAN = 1164730977;
    GRANTHA = 1198678382;
    KHOJKI = 1265135466;
    KHUDAWADI = 1399418468;
    LINEAR_A = 1281977953;
    MAHAJANI = 1298229354;
    MANICHAEAN = 1298230889;
    MENDE_KIKAKUI = 1298493028;
    MODI = 1299145833;
    MRO = 1299345263;
    NABATAEAN = 1315070324;
    OLD_NORTH_ARABIAN = 1315009122;
    OLD_PERMIC = 1348825709;
    PAHAWH_HMONG = 1215131239;
    PALMYRENE = 1348562029;
    PAU_CIN_HAU = 1348564323;
    PSALTER_PAHLAVI = 1349020784;
    SIDDHAM = 1399415908;
    TIRHUTA = 1416196712;
    WARANG_CITI = 1466004065;
    AHOM = 1097363309;
    ANATOLIAN_HIEROGLYPHS = 1215067511;
    HATRAN = 1214346354;
    MULTANI = 1299541108;
    OLD_HUNGARIAN = 1215655527;
    SIGNWRITING = 1399287415;
    ADLAM = 1097100397;
    BHAIKSUKI = 1114139507;
    MARCHEN = 1298231907;
    OSAGE = 1332963173;
    TANGUT = 1415671399;
    NEWA = 1315272545;
    MASARAM_GONDI = 1198485101;
    NUSHU = 1316186229;
    SOYOMBO = 1399814511;
    ZANABAZAR_SQUARE = 1516334690;
    DOGRA = 1148151666;
    GUNJALA_GONDI = 1198485095;
    HANIFI_ROHINGYA = 1383032935;
    MAKASAR = 1298230113;
    MEDEFAIDRIN = 1298490470;
    OLD_SOGDIAN = 1399809903;
    SOGDIAN = 1399809892;
    INVALID = 0;
}
impl_enum! {
    MemoryMode;
    DUPLICATE = 0;
    READONLY = 1;
    WRITABLE = 2;
    READONLY_MAY_MAKE_WRITABLE = 3;
}
impl_enum! {
    UnicodeGeneralCategory;
    CONTROL = 0;
    FORMAT = 1;
    UNASSIGNED = 2;
    PRIVATE_USE = 3;
    SURROGATE = 4;
    LOWERCASE_LETTER = 5;
    MODIFIER_LETTER = 6;
    OTHER_LETTER = 7;
    TITLECASE_LETTER = 8;
    UPPERCASE_LETTER = 9;
    SPACING_MARK = 10;
    ENCLOSING_MARK = 11;
    NON_SPACING_MARK = 12;
    DECIMAL_NUMBER = 13;
    LETTER_NUMBER = 14;
    OTHER_NUMBER = 15;
    CONNECT_PUNCTUATION = 16;
    DASH_PUNCTUATION = 17;
    CLOSE_PUNCTUATION = 18;
    FINAL_PUNCTUATION = 19;
    INITIAL_PUNCTUATION = 20;
    OTHER_PUNCTUATION = 21;
    OPEN_PUNCTUATION = 22;
    CURRENCY_SYMBOL = 23;
    MODIFIER_SYMBOL = 24;
    MATH_SYMBOL = 25;
    OTHER_SYMBOL = 26;
    LINE_SEPARATOR = 27;
    PARAGRAPH_SEPARATOR = 28;
    SPACE_SEPARATOR = 29;
}
impl_enum! {
    UnicodeCombiningClass;
    NOT_REORDERED = 0;
    OVERLAY = 1;
    NUKTA = 7;
    KANA_VOICING = 8;
    VIRAMA = 9;
    CCC10 = 10;
    CCC11 = 11;
    CCC12 = 12;
    CCC13 = 13;
    CCC14 = 14;
    CCC15 = 15;
    CCC16 = 16;
    CCC17 = 17;
    CCC18 = 18;
    CCC19 = 19;
    CCC20 = 20;
    CCC21 = 21;
    CCC22 = 22;
    CCC23 = 23;
    CCC24 = 24;
    CCC25 = 25;
    CCC26 = 26;
    CCC27 = 27;
    CCC28 = 28;
    CCC29 = 29;
    CCC30 = 30;
    CCC31 = 31;
    CCC32 = 32;
    CCC33 = 33;
    CCC34 = 34;
    CCC35 = 35;
    CCC36 = 36;
    CCC84 = 84;
    CCC91 = 91;
    CCC103 = 103;
    CCC107 = 107;
    CCC118 = 118;
    CCC122 = 122;
    CCC129 = 129;
    CCC130 = 130;
    CCC133 = 132;
    ATTACHED_BELOW_LEFT = 200;
    ATTACHED_BELOW = 202;
    ATTACHED_ABOVE = 214;
    ATTACHED_ABOVE_RIGHT = 216;
    BELOW_LEFT = 218;
    BELOW = 220;
    BELOW_RIGHT = 222;
    LEFT = 224;
    RIGHT = 226;
    ABOVE_LEFT = 228;
    ABOVE = 230;
    ABOVE_RIGHT = 232;
    DOUBLE_BELOW = 233;
    DOUBLE_ABOVE = 234;
    IOTA_SUBSCRIPT = 240;
    INVALID = 255;
}
impl_enum! {
    BufferContentType;
    INVALID = 0;
    UNICODE = 1;
    GLYPHS = 2;
}
impl_enum! {
    BufferClusterLevel;
    MONOTONE_GRAPHEMES = 0;
    MONOTONE_CHARACTERS = 1;
    CHARACTERS = 2;
    DEFAULT = 0;
}
impl_enum! {
    BufferSerializeFormat;
    TEXT = 1413830740;
    JSON = 1246973774;
    INVALID = 0;
}

macro_rules! impl_flags {
    ($name:ident; $($mem:ident = $val:expr;)*) => {
        impl_flags!($name(u32); $($mem = $val;)*);
    };
    ($name:ident($type:ty); $($mem:ident = $val:expr;)*) => {
        impl_enum!($name($type); $($mem = $val;)*);
        impl $name {
            pub fn empty() -> Self { $name(0) }
            pub fn is_empty(self) -> bool { self.0 == 0 }
            pub fn contains(self, other: Self) -> bool
                { self & other == other }
            pub fn intersects(self, other: Self) -> bool
                { !(self & other).is_empty() }
        }
        impl Not for $name {
            type Output = Self;
            fn not(self) -> Self { $name(self.0.not()) }
        }
        impl BitAnd for $name {
            type Output = Self;
            fn bitand(self, other: Self) -> Self
                { $name(self.0.bitand(other.0)) }
        }
        impl BitOr for $name {
            type Output = Self;
            fn bitor(self, other: Self) -> Self
                { $name(self.0.bitor(other.0)) }
        }
        impl BitXor for $name {
            type Output = Self;
            fn bitxor(self, other: Self) -> Self
                { $name(self.0.bitxor(other.0)) }
        }
        impl BitAndAssign for $name {
            fn bitand_assign(&mut self, other: Self)
                { self.0.bitand_assign(other.0) }
        }
        impl BitOrAssign for $name {
            fn bitor_assign(&mut self, other: Self)
                { self.0.bitor_assign(other.0) }
        }
        impl BitXorAssign for $name {
            fn bitxor_assign(&mut self, other: Self)
                { self.0.bitxor_assign(other.0) }
        }
    };
}

impl_flags! {
    GlyphFlags;
    UNSAFE_TO_BREAK = 1;
    DEFINED = 1;
}
impl_flags! {
    BufferFlags;
    DEFAULT = 0;
    BOT = 1;
    EOT = 2;
    PRESERVE_DEFAULT_IGNORABLES = 4;
    REMOVE_DEFAULT_IGNORABLES = 8;
}
impl_flags! {
    BufferSerializeFlags;
    DEFAULT = 0;
    NO_CLUSTERS = 1;
    NO_POSITIONS = 2;
    NO_GLYPH_NAMES = 4;
    GLYPH_EXTENTS = 8;
    GLYPH_FLAGS = 16;
    NO_ADVANCES = 32;
}
impl_flags! {
    BufferDiffFlags;
    EQUAL = 0;
    CONTENT_TYPE_MISMATCH = 1;
    LENGTH_MISMATCH = 2;
    NOTDEF_PRESENT = 4;
    DOTTED_CIRCLE_PRESENT = 8;
    CODEPOINT_MISMATCH = 16;
    CLUSTER_MISMATCH = 32;
    GLYPH_FLAGS_MISMATCH = 64;
    POSITION_MISMATCH = 128;
}

extern {
    pub type LanguageImpl;
    pub type UserDataKey;
    pub type Buffer;
    pub type Map;
    pub type ShapePlan;
    pub type Blob;
    pub type UnicodeFuncs;
    pub type Set;
    pub type Face;
    pub type Font;
    pub type FontFuncs;
}

macro_rules! impl_struct {
    ($name:ident; $($field:ident: $type:ty,)*) => {
        #[repr(C)]
        #[derive(Clone, Copy, Debug)]
        pub struct $name { $(pub $field: $type,)* }
        impl Default for $name {
            fn default() -> Self { unsafe { std::mem::zeroed() } }
        }
    }
}

impl_struct! {
    Feature;
    tag: Tag,
    value: u32,
    start: c_uint,
    end: c_uint,
}
impl_struct! {
    Variation;
    tag: Tag,
    value: f32,
}
impl_struct! {
    FontExtents;
    ascender: Position,
    descender: Position,
    line_gap: Position,
    reserved9: Position,
    reserved8: Position,
    reserved7: Position,
    reserved6: Position,
    reserved5: Position,
    reserved4: Position,
    reserved3: Position,
    reserved2: Position,
    reserved1: Position,
}
impl_struct! {
    GlyphExtents;
    x_bearing: Position,
    y_bearing: Position,
    width: Position,
    height: Position,
}
impl_struct! {
    GlyphInfo;
    codepoint: Codepoint,
    mask: Mask,
    cluster: u32,
    var1: VarInt,
    var2: VarInt,
}
impl_struct! {
    GlyphPosition;
    x_advance: Position,
    y_advance: Position,
    x_offset: Position,
    y_offset: Position,
    var: VarInt,
}
impl_struct! {
    SegmentProperties;
    direction: Direction,
    script: Script,
    language: Language,
    reserved1: *mut c_void,
    reserved2: *mut c_void,
}

macro_rules! impl_union {
    ($(#[$meta:meta])* $name:ident; $($field:ident: $type:ty,)*) => {
        $(#[$meta])*
        #[repr(C)]
        #[derive(Clone, Copy)]
        pub union $name { $(pub $field: $type,)* }
        impl Default for $name {
            fn default() -> Self { unsafe { std::mem::zeroed() } }
        }
        impl std::fmt::Debug for VarInt {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                write!(f, concat!(stringify!($name), " {{ *union* }}"))
            }
        }
    }
}

impl_union! {
    #[repr(align(4))]
    VarInt;
    u32_: u32,
    i32_: i32,
    u16_: [u16; 2usize],
    i16_: [i16; 2usize],
    u8_: [u8; 4usize],
    i8_: [i8; 4usize],
}

pub type FontGetFontHExtentsFunc = FontGetFontExtentsFunc;
pub type FontGetFontVExtentsFunc = FontGetFontExtentsFunc;
pub type FontGetGlyphHAdvanceFunc = FontGetGlyphAdvanceFunc;
pub type FontGetGlyphVAdvanceFunc = FontGetGlyphAdvanceFunc;
pub type FontGetGlyphHAdvancesFunc = FontGetGlyphAdvancesFunc;
pub type FontGetGlyphVAdvancesFunc = FontGetGlyphAdvancesFunc;
pub type FontGetGlyphHOriginFunc = FontGetGlyphOriginFunc;
pub type FontGetGlyphVOriginFunc = FontGetGlyphOriginFunc;
pub type FontGetGlyphHKerningFunc = FontGetGlyphKerningFunc;
pub type FontGetGlyphVKerningFunc = FontGetGlyphKerningFunc;

macro_rules! fn_ptr {
    (($($param:ident: $type:ty,)*)) => {
        Option<unsafe extern "C" fn($($param: $type,)*)>
    };
    (($($param:ident: $type:ty,)*) -> $ret:ty) => {
        Option<unsafe extern "C" fn($($param: $type,)*) -> $ret>
    };
}

pub type DestroyFunc = fn_ptr!((
    user_data: *mut c_void,
));
pub type UnicodeCombiningClassFunc = fn_ptr!((
    ufuncs: *mut UnicodeFuncs,
    unicode: Codepoint,
    user_data: *mut c_void,
) -> UnicodeCombiningClass);
pub type UnicodeGeneralCategoryFunc = fn_ptr!((
    ufuncs: *mut UnicodeFuncs,
    unicode: Codepoint,
    user_data: *mut c_void,
) -> UnicodeGeneralCategory);
pub type UnicodeMirroringFunc = fn_ptr!((
    ufuncs: *mut UnicodeFuncs,
    unicode: Codepoint,
    user_data: *mut c_void,
) -> Codepoint);
pub type UnicodeScriptFunc = fn_ptr!((
    ufuncs: *mut UnicodeFuncs,
    unicode: Codepoint,
    user_data: *mut c_void,
) -> Script);
pub type UnicodeComposeFunc = fn_ptr!((
    ufuncs: *mut UnicodeFuncs,
    a: Codepoint,
    b: Codepoint,
    ab: *mut Codepoint,
    user_data: *mut c_void,
) -> Bool);
pub type UnicodeDecomposeFunc = fn_ptr!((
    ufuncs: *mut UnicodeFuncs,
    ab: Codepoint,
    a: *mut Codepoint,
    b: *mut Codepoint,
    user_data: *mut c_void,
) -> Bool);
pub type ReferenceTableFunc = fn_ptr!((
    face: *mut Face,
    tag: Tag,
    user_data: *mut c_void,
) -> *mut Blob);
pub type FontGetFontExtentsFunc = fn_ptr!((
    font: *mut Font,
    font_data: *mut c_void,
    extents: *mut FontExtents,
    user_data: *mut c_void,
) -> Bool);
pub type FontGetNominalGlyphFunc = fn_ptr!((
    font: *mut Font,
    font_data: *mut c_void,
    unicode: Codepoint,
    glyph: *mut Codepoint,
    user_data: *mut c_void,
) -> Bool);
pub type FontGetVariationGlyphFunc = fn_ptr!((
    font: *mut Font,
    font_data: *mut c_void,
    unicode: Codepoint,
    variation_selector: Codepoint,
    glyph: *mut Codepoint,
    user_data: *mut c_void,
) -> Bool);
pub type FontGetNominalGlyphsFunc = fn_ptr!((
    font: *mut Font,
    font_data: *mut c_void,
    count: c_uint,
    first_unicode: *const Codepoint,
    unicode_stride: c_uint,
    first_glyph: *mut Codepoint,
    glyph_stride: c_uint,
    user_data: *mut c_void,
) -> c_uint);
pub type FontGetGlyphAdvanceFunc = fn_ptr!((
    font: *mut Font,
    font_data: *mut c_void,
    glyph: Codepoint,
    user_data: *mut c_void,
) -> Position);
pub type FontGetGlyphAdvancesFunc = fn_ptr!((
    font: *mut Font,
    font_data: *mut c_void,
    count: c_uint,
    first_glyph: *const Codepoint,
    glyph_stride: c_uint,
    first_advance: *mut Position,
    advance_stride: c_uint,
    user_data: *mut c_void,
));
pub type FontGetGlyphOriginFunc = fn_ptr!((
    font: *mut Font,
    font_data: *mut c_void,
    glyph: Codepoint,
    x: *mut Position,
    y: *mut Position,
    user_data: *mut c_void,
) -> Bool);
pub type FontGetGlyphExtentsFunc = fn_ptr!((
    font: *mut Font,
    font_data: *mut c_void,
    glyph: Codepoint,
    extents: *mut GlyphExtents,
    user_data: *mut c_void,
) -> Bool);
pub type FontGetGlyphContourPointFunc = fn_ptr!((
    font: *mut Font,
    font_data: *mut c_void,
    glyph: Codepoint,
    point_index: c_uint,
    x: *mut Position,
    y: *mut Position,
    user_data: *mut c_void,
) -> Bool);
pub type FontGetGlyphNameFunc = fn_ptr!((
    font: *mut Font,
    font_data: *mut c_void,
    glyph: Codepoint,
    name: *mut c_char,
    size: c_uint,
    user_data: *mut c_void,
) -> Bool);
pub type FontGetGlyphFromNameFunc = fn_ptr!((
    font: *mut Font,
    font_data: *mut c_void,
    name: *const c_char,
    len: c_int,
    glyph: *mut Codepoint,
    user_data: *mut c_void,
) -> Bool);
pub type BufferMessageFunc = fn_ptr!((
    buffer: *mut Buffer,
    font: *mut Font,
    message: *const c_char,
    user_data: *mut c_void,
) -> Bool);
pub type FontGetGlyphFunc = fn_ptr!((
    font: *mut Font,
    font_data: *mut c_void,
    unicode: Codepoint,
    variation_selector: Codepoint,
    glyph: *mut Codepoint,
    user_data: *mut c_void,
) -> Bool);
pub type UnicodeEastasianWidthFunc = fn_ptr!((
    ufuncs: *mut UnicodeFuncs,
    unicode: Codepoint,
    user_data: *mut c_void,
) -> c_uint);
pub type UnicodeDecomposeCompatibilityFunc = fn_ptr!((
    ufuncs: *mut UnicodeFuncs,
    u: Codepoint,
    decomposed: *mut Codepoint,
    user_data: *mut c_void,
) -> c_uint);
pub type FontGetGlyphKerningFunc = fn_ptr!((
    font: *mut Font,
    font_data: *mut c_void,
    first_glyph: Codepoint,
    second_glyph: Codepoint,
    user_data: *mut c_void,
) -> Position);

#[link(name = "harfbuzz")]
extern "C" {
    #[link_name = "hb_tag_from_string"]
    pub fn tag_from_string(
        str: *const c_char,
        len: c_int,
    ) -> Tag;
    #[link_name = "hb_tag_to_string"]
    pub fn tag_to_string(
        tag: Tag,
        buf: *mut c_char,
    );
    #[link_name = "hb_direction_from_string"]
    pub fn direction_from_string(
        str: *const c_char,
        len: c_int,
    ) -> Direction;
    #[link_name = "hb_direction_to_string"]
    pub fn direction_to_string(
        direction: Direction,
    ) -> *const c_char;
    #[link_name = "hb_language_from_string"]
    pub fn language_from_string(
        str: *const c_char,
        len: c_int,
    ) -> Language;
    #[link_name = "hb_language_to_string"]
    pub fn language_to_string(
        language: Language,
    ) -> *const c_char;
    #[link_name = "hb_language_get_default"]
    pub fn language_get_default(
    ) -> Language;
    #[link_name = "hb_script_from_iso15924_tag"]
    pub fn script_from_iso15924_tag(
        tag: Tag,
    ) -> Script;
    #[link_name = "hb_script_from_string"]
    pub fn script_from_string(
        str: *const c_char,
        len: c_int,
    ) -> Script;
    #[link_name = "hb_script_to_iso15924_tag"]
    pub fn script_to_iso15924_tag(
        script: Script,
    ) -> Tag;
    #[link_name = "hb_script_get_horizontal_direction"]
    pub fn script_get_horizontal_direction(
        script: Script,
    ) -> Direction;
    #[link_name = "hb_feature_from_string"]
    pub fn feature_from_string(
        str: *const c_char,
        len: c_int,
        feature: *mut Feature,
    ) -> Bool;
    #[link_name = "hb_feature_to_string"]
    pub fn feature_to_string(
        feature: *mut Feature,
        buf: *mut c_char,
        size: c_uint,
    );
    #[link_name = "hb_variation_from_string"]
    pub fn variation_from_string(
        str: *const c_char,
        len: c_int,
        variation: *mut Variation,
    ) -> Bool;
    #[link_name = "hb_variation_to_string"]
    pub fn variation_to_string(
        variation: *mut Variation,
        buf: *mut c_char,
        size: c_uint,
    );
    #[link_name = "hb_blob_create"]
    pub fn blob_create(
        data: *const c_char,
        length: c_uint,
        mode: MemoryMode,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    ) -> *mut Blob;
    #[link_name = "hb_blob_create_sub_blob"]
    pub fn blob_create_sub_blob(
        parent: *mut Blob,
        offset: c_uint,
        length: c_uint,
    ) -> *mut Blob;
    #[link_name = "hb_blob_copy_writable_or_fail"]
    pub fn blob_copy_writable_or_fail(
        blob: *mut Blob,
    ) -> *mut Blob;
    #[link_name = "hb_blob_get_empty"]
    pub fn blob_get_empty(
    ) -> *mut Blob;
    #[link_name = "hb_blob_reference"]
    pub fn blob_reference(
        blob: *mut Blob,
    ) -> *mut Blob;
    #[link_name = "hb_blob_destroy"]
    pub fn blob_destroy(
        blob: *mut Blob,
    );
    #[link_name = "hb_blob_set_user_data"]
    pub fn blob_set_user_data(
        blob: *mut Blob,
        key: *mut UserDataKey,
        data: *mut c_void,
        destroy: DestroyFunc,
        replace: Bool,
    ) -> Bool;
    #[link_name = "hb_blob_get_user_data"]
    pub fn blob_get_user_data(
        blob: *mut Blob,
        key: *mut UserDataKey,
    ) -> *mut c_void;
    #[link_name = "hb_blob_make_immutable"]
    pub fn blob_make_immutable(
        blob: *mut Blob,
    );
    #[link_name = "hb_blob_is_immutable"]
    pub fn blob_is_immutable(
        blob: *mut Blob,
    ) -> Bool;
    #[link_name = "hb_blob_get_length"]
    pub fn blob_get_length(
        blob: *mut Blob,
    ) -> c_uint;
    #[link_name = "hb_blob_get_data"]
    pub fn blob_get_data(
        blob: *mut Blob,
        length: *mut c_uint,
    ) -> *const c_char;
    #[link_name = "hb_blob_get_data_writable"]
    pub fn blob_get_data_writable(
        blob: *mut Blob,
        length: *mut c_uint,
    ) -> *mut c_char;
    #[link_name = "hb_blob_create_from_file"]
    pub fn blob_create_from_file(
        file_name: *const c_char,
    ) -> *mut Blob;
    #[link_name = "hb_unicode_funcs_get_default"]
    pub fn unicode_funcs_get_default(
    ) -> *mut UnicodeFuncs;
    #[link_name = "hb_unicode_funcs_create"]
    pub fn unicode_funcs_create(
        parent: *mut UnicodeFuncs,
    ) -> *mut UnicodeFuncs;
    #[link_name = "hb_unicode_funcs_get_empty"]
    pub fn unicode_funcs_get_empty(
    ) -> *mut UnicodeFuncs;
    #[link_name = "hb_unicode_funcs_reference"]
    pub fn unicode_funcs_reference(
        ufuncs: *mut UnicodeFuncs,
    ) -> *mut UnicodeFuncs;
    #[link_name = "hb_unicode_funcs_destroy"]
    pub fn unicode_funcs_destroy(
        ufuncs: *mut UnicodeFuncs,
    );
    #[link_name = "hb_unicode_funcs_set_user_data"]
    pub fn unicode_funcs_set_user_data(
        ufuncs: *mut UnicodeFuncs,
        key: *mut UserDataKey,
        data: *mut c_void,
        destroy: DestroyFunc,
        replace: Bool,
    ) -> Bool;
    #[link_name = "hb_unicode_funcs_get_user_data"]
    pub fn unicode_funcs_get_user_data(
        ufuncs: *mut UnicodeFuncs,
        key: *mut UserDataKey,
    ) -> *mut c_void;
    #[link_name = "hb_unicode_funcs_make_immutable"]
    pub fn unicode_funcs_make_immutable(
        ufuncs: *mut UnicodeFuncs,
    );
    #[link_name = "hb_unicode_funcs_is_immutable"]
    pub fn unicode_funcs_is_immutable(
        ufuncs: *mut UnicodeFuncs,
    ) -> Bool;
    #[link_name = "hb_unicode_funcs_get_parent"]
    pub fn unicode_funcs_get_parent(
        ufuncs: *mut UnicodeFuncs,
    ) -> *mut UnicodeFuncs;
    #[link_name = "hb_unicode_funcs_set_combining_class_func"]
    pub fn unicode_funcs_set_combining_class_func(
        ufuncs: *mut UnicodeFuncs,
        func: UnicodeCombiningClassFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_unicode_funcs_set_general_category_func"]
    pub fn unicode_funcs_set_general_category_func(
        ufuncs: *mut UnicodeFuncs,
        func: UnicodeGeneralCategoryFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_unicode_funcs_set_mirroring_func"]
    pub fn unicode_funcs_set_mirroring_func(
        ufuncs: *mut UnicodeFuncs,
        func: UnicodeMirroringFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_unicode_funcs_set_script_func"]
    pub fn unicode_funcs_set_script_func(
        ufuncs: *mut UnicodeFuncs,
        func: UnicodeScriptFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_unicode_funcs_set_compose_func"]
    pub fn unicode_funcs_set_compose_func(
        ufuncs: *mut UnicodeFuncs,
        func: UnicodeComposeFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_unicode_funcs_set_decompose_func"]
    pub fn unicode_funcs_set_decompose_func(
        ufuncs: *mut UnicodeFuncs,
        func: UnicodeDecomposeFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_unicode_combining_class"]
    pub fn unicode_combining_class(
        ufuncs: *mut UnicodeFuncs,
        unicode: Codepoint,
    ) -> UnicodeCombiningClass;
    #[link_name = "hb_unicode_general_category"]
    pub fn unicode_general_category(
        ufuncs: *mut UnicodeFuncs,
        unicode: Codepoint,
    ) -> UnicodeGeneralCategory;
    #[link_name = "hb_unicode_mirroring"]
    pub fn unicode_mirroring(
        ufuncs: *mut UnicodeFuncs,
        unicode: Codepoint,
    ) -> Codepoint;
    #[link_name = "hb_unicode_script"]
    pub fn unicode_script(
        ufuncs: *mut UnicodeFuncs,
        unicode: Codepoint,
    ) -> Script;
    #[link_name = "hb_unicode_compose"]
    pub fn unicode_compose(
        ufuncs: *mut UnicodeFuncs,
        a: Codepoint,
        b: Codepoint,
        ab: *mut Codepoint,
    ) -> Bool;
    #[link_name = "hb_unicode_decompose"]
    pub fn unicode_decompose(
        ufuncs: *mut UnicodeFuncs,
        ab: Codepoint,
        a: *mut Codepoint,
        b: *mut Codepoint,
    ) -> Bool;
    #[link_name = "hb_set_create"]
    pub fn set_create(
    ) -> *mut Set;
    #[link_name = "hb_set_get_empty"]
    pub fn set_get_empty(
    ) -> *mut Set;
    #[link_name = "hb_set_reference"]
    pub fn set_reference(
        set: *mut Set,
    ) -> *mut Set;
    #[link_name = "hb_set_destroy"]
    pub fn set_destroy(
        set: *mut Set,
    );
    #[link_name = "hb_set_set_user_data"]
    pub fn set_set_user_data(
        set: *mut Set,
        key: *mut UserDataKey,
        data: *mut c_void,
        destroy: DestroyFunc,
        replace: Bool,
    ) -> Bool;
    #[link_name = "hb_set_get_user_data"]
    pub fn set_get_user_data(
        set: *mut Set,
        key: *mut UserDataKey,
    ) -> *mut c_void;
    #[link_name = "hb_set_allocation_successful"]
    pub fn set_allocation_successful(
        set: *const Set,
    ) -> Bool;
    #[link_name = "hb_set_clear"]
    pub fn set_clear(
        set: *mut Set,
    );
    #[link_name = "hb_set_is_empty"]
    pub fn set_is_empty(
        set: *const Set,
    ) -> Bool;
    #[link_name = "hb_set_has"]
    pub fn set_has(
        set: *const Set,
        codepoint: Codepoint,
    ) -> Bool;
    #[link_name = "hb_set_add"]
    pub fn set_add(
        set: *mut Set,
        codepoint: Codepoint,
    );
    #[link_name = "hb_set_add_range"]
    pub fn set_add_range(
        set: *mut Set,
        first: Codepoint,
        last: Codepoint,
    );
    #[link_name = "hb_set_del"]
    pub fn set_del(
        set: *mut Set,
        codepoint: Codepoint,
    );
    #[link_name = "hb_set_del_range"]
    pub fn set_del_range(
        set: *mut Set,
        first: Codepoint,
        last: Codepoint,
    );
    #[link_name = "hb_set_is_equal"]
    pub fn set_is_equal(
        set: *const Set,
        other: *const Set,
    ) -> Bool;
    #[link_name = "hb_set_is_subset"]
    pub fn set_is_subset(
        set: *const Set,
        larger_set: *const Set,
    ) -> Bool;
    #[link_name = "hb_set_set"]
    pub fn set_set(
        set: *mut Set,
        other: *const Set,
    );
    #[link_name = "hb_set_union"]
    pub fn set_union(
        set: *mut Set,
        other: *const Set,
    );
    #[link_name = "hb_set_intersect"]
    pub fn set_intersect(
        set: *mut Set,
        other: *const Set,
    );
    #[link_name = "hb_set_subtract"]
    pub fn set_subtract(
        set: *mut Set,
        other: *const Set,
    );
    #[link_name = "hb_set_symmetric_difference"]
    pub fn set_symmetric_difference(
        set: *mut Set,
        other: *const Set,
    );
    #[link_name = "hb_set_get_population"]
    pub fn set_get_population(
        set: *const Set,
    ) -> c_uint;
    #[link_name = "hb_set_get_min"]
    pub fn set_get_min(
        set: *const Set,
    ) -> Codepoint;
    #[link_name = "hb_set_get_max"]
    pub fn set_get_max(
        set: *const Set,
    ) -> Codepoint;
    #[link_name = "hb_set_next"]
    pub fn set_next(
        set: *const Set,
        codepoint: *mut Codepoint,
    ) -> Bool;
    #[link_name = "hb_set_previous"]
    pub fn set_previous(
        set: *const Set,
        codepoint: *mut Codepoint,
    ) -> Bool;
    #[link_name = "hb_set_next_range"]
    pub fn set_next_range(
        set: *const Set,
        first: *mut Codepoint,
        last: *mut Codepoint,
    ) -> Bool;
    #[link_name = "hb_set_previous_range"]
    pub fn set_previous_range(
        set: *const Set,
        first: *mut Codepoint,
        last: *mut Codepoint,
    ) -> Bool;
    #[link_name = "hb_face_count"]
    pub fn face_count(
        blob: *mut Blob,
    ) -> c_uint;
    #[link_name = "hb_face_create"]
    pub fn face_create(
        blob: *mut Blob,
        index: c_uint,
    ) -> *mut Face;
    #[link_name = "hb_face_create_for_tables"]
    pub fn face_create_for_tables(
        reference_table_func: ReferenceTableFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    ) -> *mut Face;
    #[link_name = "hb_face_get_empty"]
    pub fn face_get_empty(
    ) -> *mut Face;
    #[link_name = "hb_face_reference"]
    pub fn face_reference(
        face: *mut Face,
    ) -> *mut Face;
    #[link_name = "hb_face_destroy"]
    pub fn face_destroy(
        face: *mut Face,
    );
    #[link_name = "hb_face_set_user_data"]
    pub fn face_set_user_data(
        face: *mut Face,
        key: *mut UserDataKey,
        data: *mut c_void,
        destroy: DestroyFunc,
        replace: Bool,
    ) -> Bool;
    #[link_name = "hb_face_get_user_data"]
    pub fn face_get_user_data(
        face: *const Face,
        key: *mut UserDataKey,
    ) -> *mut c_void;
    #[link_name = "hb_face_make_immutable"]
    pub fn face_make_immutable(
        face: *mut Face,
    );
    #[link_name = "hb_face_is_immutable"]
    pub fn face_is_immutable(
        face: *const Face,
    ) -> Bool;
    #[link_name = "hb_face_reference_table"]
    pub fn face_reference_table(
        face: *const Face,
        tag: Tag,
    ) -> *mut Blob;
    #[link_name = "hb_face_reference_blob"]
    pub fn face_reference_blob(
        face: *mut Face,
    ) -> *mut Blob;
    #[link_name = "hb_face_set_index"]
    pub fn face_set_index(
        face: *mut Face,
        index: c_uint,
    );
    #[link_name = "hb_face_get_index"]
    pub fn face_get_index(
        face: *const Face,
    ) -> c_uint;
    #[link_name = "hb_face_set_upem"]
    pub fn face_set_upem(
        face: *mut Face,
        upem: c_uint,
    );
    #[link_name = "hb_face_get_upem"]
    pub fn face_get_upem(
        face: *const Face,
    ) -> c_uint;
    #[link_name = "hb_face_set_glyph_count"]
    pub fn face_set_glyph_count(
        face: *mut Face,
        glyph_count: c_uint,
    );
    #[link_name = "hb_face_get_glyph_count"]
    pub fn face_get_glyph_count(
        face: *const Face,
    ) -> c_uint;
    #[link_name = "hb_face_get_table_tags"]
    pub fn face_get_table_tags(
        face: *const Face,
        start_offset: c_uint,
        table_count: *mut c_uint,
        table_tags: *mut Tag,
    ) -> c_uint;
    #[link_name = "hb_face_collect_unicodes"]
    pub fn face_collect_unicodes(
        face: *mut Face,
        out: *mut Set,
    );
    #[link_name = "hb_face_collect_variation_selectors"]
    pub fn face_collect_variation_selectors(
        face: *mut Face,
        out: *mut Set,
    );
    #[link_name = "hb_face_collect_variation_unicodes"]
    pub fn face_collect_variation_unicodes(
        face: *mut Face,
        variation_selector: Codepoint,
        out: *mut Set,
    );
    #[link_name = "hb_face_builder_create"]
    pub fn face_builder_create(
    ) -> *mut Face;
    #[link_name = "hb_face_builder_add_table"]
    pub fn face_builder_add_table(
        face: *mut Face,
        tag: Tag,
        blob: *mut Blob,
    ) -> Bool;
    #[link_name = "hb_font_funcs_create"]
    pub fn font_funcs_create(
    ) -> *mut FontFuncs;
    #[link_name = "hb_font_funcs_get_empty"]
    pub fn font_funcs_get_empty(
    ) -> *mut FontFuncs;
    #[link_name = "hb_font_funcs_reference"]
    pub fn font_funcs_reference(
        ffuncs: *mut FontFuncs,
    ) -> *mut FontFuncs;
    #[link_name = "hb_font_funcs_destroy"]
    pub fn font_funcs_destroy(
        ffuncs: *mut FontFuncs,
    );
    #[link_name = "hb_font_funcs_set_user_data"]
    pub fn font_funcs_set_user_data(
        ffuncs: *mut FontFuncs,
        key: *mut UserDataKey,
        data: *mut c_void,
        destroy: DestroyFunc,
        replace: Bool,
    ) -> Bool;
    #[link_name = "hb_font_funcs_get_user_data"]
    pub fn font_funcs_get_user_data(
        ffuncs: *mut FontFuncs,
        key: *mut UserDataKey,
    ) -> *mut c_void;
    #[link_name = "hb_font_funcs_make_immutable"]
    pub fn font_funcs_make_immutable(
        ffuncs: *mut FontFuncs,
    );
    #[link_name = "hb_font_funcs_is_immutable"]
    pub fn font_funcs_is_immutable(
        ffuncs: *mut FontFuncs,
    ) -> Bool;
    #[link_name = "hb_font_funcs_set_font_h_extents_func"]
    pub fn font_funcs_set_font_h_extents_func(
        ffuncs: *mut FontFuncs,
        func: FontGetFontHExtentsFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_font_v_extents_func"]
    pub fn font_funcs_set_font_v_extents_func(
        ffuncs: *mut FontFuncs,
        func: FontGetFontVExtentsFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_nominal_glyph_func"]
    pub fn font_funcs_set_nominal_glyph_func(
        ffuncs: *mut FontFuncs,
        func: FontGetNominalGlyphFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_nominal_glyphs_func"]
    pub fn font_funcs_set_nominal_glyphs_func(
        ffuncs: *mut FontFuncs,
        func: FontGetNominalGlyphsFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_variation_glyph_func"]
    pub fn font_funcs_set_variation_glyph_func(
        ffuncs: *mut FontFuncs,
        func: FontGetVariationGlyphFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_glyph_h_advance_func"]
    pub fn font_funcs_set_glyph_h_advance_func(
        ffuncs: *mut FontFuncs,
        func: FontGetGlyphHAdvanceFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_glyph_v_advance_func"]
    pub fn font_funcs_set_glyph_v_advance_func(
        ffuncs: *mut FontFuncs,
        func: FontGetGlyphVAdvanceFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_glyph_h_advances_func"]
    pub fn font_funcs_set_glyph_h_advances_func(
        ffuncs: *mut FontFuncs,
        func: FontGetGlyphHAdvancesFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_glyph_v_advances_func"]
    pub fn font_funcs_set_glyph_v_advances_func(
        ffuncs: *mut FontFuncs,
        func: FontGetGlyphVAdvancesFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_glyph_h_origin_func"]
    pub fn font_funcs_set_glyph_h_origin_func(
        ffuncs: *mut FontFuncs,
        func: FontGetGlyphHOriginFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_glyph_v_origin_func"]
    pub fn font_funcs_set_glyph_v_origin_func(
        ffuncs: *mut FontFuncs,
        func: FontGetGlyphVOriginFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_glyph_extents_func"]
    pub fn font_funcs_set_glyph_extents_func(
        ffuncs: *mut FontFuncs,
        func: FontGetGlyphExtentsFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_glyph_contour_point_func"]
    pub fn font_funcs_set_glyph_contour_point_func(
        ffuncs: *mut FontFuncs,
        func: FontGetGlyphContourPointFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_glyph_name_func"]
    pub fn font_funcs_set_glyph_name_func(
        ffuncs: *mut FontFuncs,
        func: FontGetGlyphNameFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_glyph_from_name_func"]
    pub fn font_funcs_set_glyph_from_name_func(
        ffuncs: *mut FontFuncs,
        func: FontGetGlyphFromNameFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_get_h_extents"]
    pub fn font_get_h_extents(
        font: *mut Font,
        extents: *mut FontExtents,
    ) -> Bool;
    #[link_name = "hb_font_get_v_extents"]
    pub fn font_get_v_extents(
        font: *mut Font,
        extents: *mut FontExtents,
    ) -> Bool;
    #[link_name = "hb_font_get_nominal_glyph"]
    pub fn font_get_nominal_glyph(
        font: *mut Font,
        unicode: Codepoint,
        glyph: *mut Codepoint,
    ) -> Bool;
    #[link_name = "hb_font_get_variation_glyph"]
    pub fn font_get_variation_glyph(
        font: *mut Font,
        unicode: Codepoint,
        variation_selector: Codepoint,
        glyph: *mut Codepoint,
    ) -> Bool;
    #[link_name = "hb_font_get_glyph_h_advance"]
    pub fn font_get_glyph_h_advance(
        font: *mut Font,
        glyph: Codepoint,
    ) -> Position;
    #[link_name = "hb_font_get_glyph_v_advance"]
    pub fn font_get_glyph_v_advance(
        font: *mut Font,
        glyph: Codepoint,
    ) -> Position;
    #[link_name = "hb_font_get_glyph_h_advances"]
    pub fn font_get_glyph_h_advances(
        font: *mut Font,
        count: c_uint,
        first_glyph: *const Codepoint,
        glyph_stride: c_uint,
        first_advance: *mut Position,
        advance_stride: c_uint,
    );
    #[link_name = "hb_font_get_glyph_v_advances"]
    pub fn font_get_glyph_v_advances(
        font: *mut Font,
        count: c_uint,
        first_glyph: *const Codepoint,
        glyph_stride: c_uint,
        first_advance: *mut Position,
        advance_stride: c_uint,
    );
    #[link_name = "hb_font_get_glyph_h_origin"]
    pub fn font_get_glyph_h_origin(
        font: *mut Font,
        glyph: Codepoint,
        x: *mut Position,
        y: *mut Position,
    ) -> Bool;
    #[link_name = "hb_font_get_glyph_v_origin"]
    pub fn font_get_glyph_v_origin(
        font: *mut Font,
        glyph: Codepoint,
        x: *mut Position,
        y: *mut Position,
    ) -> Bool;
    #[link_name = "hb_font_get_glyph_extents"]
    pub fn font_get_glyph_extents(
        font: *mut Font,
        glyph: Codepoint,
        extents: *mut GlyphExtents,
    ) -> Bool;
    #[link_name = "hb_font_get_glyph_contour_point"]
    pub fn font_get_glyph_contour_point(
        font: *mut Font,
        glyph: Codepoint,
        point_index: c_uint,
        x: *mut Position,
        y: *mut Position,
    ) -> Bool;
    #[link_name = "hb_font_get_glyph_name"]
    pub fn font_get_glyph_name(
        font: *mut Font,
        glyph: Codepoint,
        name: *mut c_char,
        size: c_uint,
    ) -> Bool;
    #[link_name = "hb_font_get_glyph_from_name"]
    pub fn font_get_glyph_from_name(
        font: *mut Font,
        name: *const c_char,
        len: c_int,
        glyph: *mut Codepoint,
    ) -> Bool;
    #[link_name = "hb_font_get_glyph"]
    pub fn font_get_glyph(
        font: *mut Font,
        unicode: Codepoint,
        variation_selector: Codepoint,
        glyph: *mut Codepoint,
    ) -> Bool;
    #[link_name = "hb_font_get_extents_for_direction"]
    pub fn font_get_extents_for_direction(
        font: *mut Font,
        direction: Direction,
        extents: *mut FontExtents,
    );
    #[link_name = "hb_font_get_glyph_advance_for_direction"]
    pub fn font_get_glyph_advance_for_direction(
        font: *mut Font,
        glyph: Codepoint,
        direction: Direction,
        x: *mut Position,
        y: *mut Position,
    );
    #[link_name = "hb_font_get_glyph_advances_for_direction"]
    pub fn font_get_glyph_advances_for_direction(
        font: *mut Font,
        direction: Direction,
        count: c_uint,
        first_glyph: *const Codepoint,
        glyph_stride: c_uint,
        first_advance: *mut Position,
        advance_stride: c_uint,
    );
    #[link_name = "hb_font_get_glyph_origin_for_direction"]
    pub fn font_get_glyph_origin_for_direction(
        font: *mut Font,
        glyph: Codepoint,
        direction: Direction,
        x: *mut Position,
        y: *mut Position,
    );
    #[link_name = "hb_font_add_glyph_origin_for_direction"]
    pub fn font_add_glyph_origin_for_direction(
        font: *mut Font,
        glyph: Codepoint,
        direction: Direction,
        x: *mut Position,
        y: *mut Position,
    );
    #[link_name = "hb_font_subtract_glyph_origin_for_direction"]
    pub fn font_subtract_glyph_origin_for_direction(
        font: *mut Font,
        glyph: Codepoint,
        direction: Direction,
        x: *mut Position,
        y: *mut Position,
    );
    #[link_name = "hb_font_get_glyph_extents_for_origin"]
    pub fn font_get_glyph_extents_for_origin(
        font: *mut Font,
        glyph: Codepoint,
        direction: Direction,
        extents: *mut GlyphExtents,
    ) -> Bool;
    #[link_name = "hb_font_get_glyph_contour_point_for_origin"]
    pub fn font_get_glyph_contour_point_for_origin(
        font: *mut Font,
        glyph: Codepoint,
        point_index: c_uint,
        direction: Direction,
        x: *mut Position,
        y: *mut Position,
    ) -> Bool;
    #[link_name = "hb_font_glyph_to_string"]
    pub fn font_glyph_to_string(
        font: *mut Font,
        glyph: Codepoint,
        s: *mut c_char,
        size: c_uint,
    );
    #[link_name = "hb_font_glyph_from_string"]
    pub fn font_glyph_from_string(
        font: *mut Font,
        s: *const c_char,
        len: c_int,
        glyph: *mut Codepoint,
    ) -> Bool;
    #[link_name = "hb_font_create"]
    pub fn font_create(
        face: *mut Face,
    ) -> *mut Font;
    #[link_name = "hb_font_create_sub_font"]
    pub fn font_create_sub_font(
        parent: *mut Font,
    ) -> *mut Font;
    #[link_name = "hb_font_get_empty"]
    pub fn font_get_empty(
    ) -> *mut Font;
    #[link_name = "hb_font_reference"]
    pub fn font_reference(
        font: *mut Font,
    ) -> *mut Font;
    #[link_name = "hb_font_destroy"]
    pub fn font_destroy(
        font: *mut Font,
    );
    #[link_name = "hb_font_set_user_data"]
    pub fn font_set_user_data(
        font: *mut Font,
        key: *mut UserDataKey,
        data: *mut c_void,
        destroy: DestroyFunc,
        replace: Bool,
    ) -> Bool;
    #[link_name = "hb_font_get_user_data"]
    pub fn font_get_user_data(
        font: *mut Font,
        key: *mut UserDataKey,
    ) -> *mut c_void;
    #[link_name = "hb_font_make_immutable"]
    pub fn font_make_immutable(
        font: *mut Font,
    );
    #[link_name = "hb_font_is_immutable"]
    pub fn font_is_immutable(
        font: *mut Font,
    ) -> Bool;
    #[link_name = "hb_font_set_parent"]
    pub fn font_set_parent(
        font: *mut Font,
        parent: *mut Font,
    );
    #[link_name = "hb_font_get_parent"]
    pub fn font_get_parent(
        font: *mut Font,
    ) -> *mut Font;
    #[link_name = "hb_font_set_face"]
    pub fn font_set_face(
        font: *mut Font,
        face: *mut Face,
    );
    #[link_name = "hb_font_get_face"]
    pub fn font_get_face(
        font: *mut Font,
    ) -> *mut Face;
    #[link_name = "hb_font_set_funcs"]
    pub fn font_set_funcs(
        font: *mut Font,
        klass: *mut FontFuncs,
        font_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_set_funcs_data"]
    pub fn font_set_funcs_data(
        font: *mut Font,
        font_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_set_scale"]
    pub fn font_set_scale(
        font: *mut Font,
        x_scale: c_int,
        y_scale: c_int,
    );
    #[link_name = "hb_font_get_scale"]
    pub fn font_get_scale(
        font: *mut Font,
        x_scale: *mut c_int,
        y_scale: *mut c_int,
    );
    #[link_name = "hb_font_set_ppem"]
    pub fn font_set_ppem(
        font: *mut Font,
        x_ppem: c_uint,
        y_ppem: c_uint,
    );
    #[link_name = "hb_font_get_ppem"]
    pub fn font_get_ppem(
        font: *mut Font,
        x_ppem: *mut c_uint,
        y_ppem: *mut c_uint,
    );
    #[link_name = "hb_font_set_ptem"]
    pub fn font_set_ptem(
        font: *mut Font,
        ptem: f32,
    );
    #[link_name = "hb_font_get_ptem"]
    pub fn font_get_ptem(
        font: *mut Font,
    ) -> f32;
    #[link_name = "hb_font_set_variations"]
    pub fn font_set_variations(
        font: *mut Font,
        variations: *const Variation,
        variations_length: c_uint,
    );
    #[link_name = "hb_font_set_var_coords_design"]
    pub fn font_set_var_coords_design(
        font: *mut Font,
        coords: *const f32,
        coords_length: c_uint,
    );
    #[link_name = "hb_font_set_var_coords_normalized"]
    pub fn font_set_var_coords_normalized(
        font: *mut Font,
        coords: *const c_int,
        coords_length: c_uint,
    );
    #[link_name = "hb_font_get_var_coords_normalized"]
    pub fn font_get_var_coords_normalized(
        font: *mut Font,
        length: *mut c_uint,
    ) -> *const c_int;
    #[link_name = "hb_glyph_info_get_glyph_flags"]
    pub fn glyph_info_get_glyph_flags(
        info: *const GlyphInfo,
    ) -> GlyphFlags;
    #[link_name = "hb_segment_properties_equal"]
    pub fn segment_properties_equal(
        a: *const SegmentProperties,
        b: *const SegmentProperties,
    ) -> Bool;
    #[link_name = "hb_segment_properties_hash"]
    pub fn segment_properties_hash(
        p: *const SegmentProperties,
    ) -> c_uint;
    #[link_name = "hb_buffer_create"]
    pub fn buffer_create(
    ) -> *mut Buffer;
    #[link_name = "hb_buffer_get_empty"]
    pub fn buffer_get_empty(
    ) -> *mut Buffer;
    #[link_name = "hb_buffer_reference"]
    pub fn buffer_reference(
        buffer: *mut Buffer,
    ) -> *mut Buffer;
    #[link_name = "hb_buffer_destroy"]
    pub fn buffer_destroy(
        buffer: *mut Buffer,
    );
    #[link_name = "hb_buffer_set_user_data"]
    pub fn buffer_set_user_data(
        buffer: *mut Buffer,
        key: *mut UserDataKey,
        data: *mut c_void,
        destroy: DestroyFunc,
        replace: Bool,
    ) -> Bool;
    #[link_name = "hb_buffer_get_user_data"]
    pub fn buffer_get_user_data(
        buffer: *mut Buffer,
        key: *mut UserDataKey,
    ) -> *mut c_void;
    #[link_name = "hb_buffer_set_content_type"]
    pub fn buffer_set_content_type(
        buffer: *mut Buffer,
        content_type: BufferContentType,
    );
    #[link_name = "hb_buffer_get_content_type"]
    pub fn buffer_get_content_type(
        buffer: *mut Buffer,
    ) -> BufferContentType;
    #[link_name = "hb_buffer_set_unicode_funcs"]
    pub fn buffer_set_unicode_funcs(
        buffer: *mut Buffer,
        unicode_funcs: *mut UnicodeFuncs,
    );
    #[link_name = "hb_buffer_get_unicode_funcs"]
    pub fn buffer_get_unicode_funcs(
        buffer: *mut Buffer,
    ) -> *mut UnicodeFuncs;
    #[link_name = "hb_buffer_set_direction"]
    pub fn buffer_set_direction(
        buffer: *mut Buffer,
        direction: Direction,
    );
    #[link_name = "hb_buffer_get_direction"]
    pub fn buffer_get_direction(
        buffer: *mut Buffer,
    ) -> Direction;
    #[link_name = "hb_buffer_set_script"]
    pub fn buffer_set_script(
        buffer: *mut Buffer,
        script: Script,
    );
    #[link_name = "hb_buffer_get_script"]
    pub fn buffer_get_script(
        buffer: *mut Buffer,
    ) -> Script;
    #[link_name = "hb_buffer_set_language"]
    pub fn buffer_set_language(
        buffer: *mut Buffer,
        language: Language,
    );
    #[link_name = "hb_buffer_get_language"]
    pub fn buffer_get_language(
        buffer: *mut Buffer,
    ) -> Language;
    #[link_name = "hb_buffer_set_segment_properties"]
    pub fn buffer_set_segment_properties(
        buffer: *mut Buffer,
        props: *const SegmentProperties,
    );
    #[link_name = "hb_buffer_get_segment_properties"]
    pub fn buffer_get_segment_properties(
        buffer: *mut Buffer,
        props: *mut SegmentProperties,
    );
    #[link_name = "hb_buffer_guess_segment_properties"]
    pub fn buffer_guess_segment_properties(
        buffer: *mut Buffer,
    );
    #[link_name = "hb_buffer_set_flags"]
    pub fn buffer_set_flags(
        buffer: *mut Buffer,
        flags: BufferFlags,
    );
    #[link_name = "hb_buffer_get_flags"]
    pub fn buffer_get_flags(
        buffer: *mut Buffer,
    ) -> BufferFlags;
    #[link_name = "hb_buffer_set_cluster_level"]
    pub fn buffer_set_cluster_level(
        buffer: *mut Buffer,
        cluster_level: BufferClusterLevel,
    );
    #[link_name = "hb_buffer_get_cluster_level"]
    pub fn buffer_get_cluster_level(
        buffer: *mut Buffer,
    ) -> BufferClusterLevel;
    #[link_name = "hb_buffer_set_replacement_codepoint"]
    pub fn buffer_set_replacement_codepoint(
        buffer: *mut Buffer,
        replacement: Codepoint,
    );
    #[link_name = "hb_buffer_get_replacement_codepoint"]
    pub fn buffer_get_replacement_codepoint(
        buffer: *mut Buffer,
    ) -> Codepoint;
    #[link_name = "hb_buffer_set_invisible_glyph"]
    pub fn buffer_set_invisible_glyph(
        buffer: *mut Buffer,
        invisible: Codepoint,
    );
    #[link_name = "hb_buffer_get_invisible_glyph"]
    pub fn buffer_get_invisible_glyph(
        buffer: *mut Buffer,
    ) -> Codepoint;
    #[link_name = "hb_buffer_reset"]
    pub fn buffer_reset(
        buffer: *mut Buffer,
    );
    #[link_name = "hb_buffer_clear_contents"]
    pub fn buffer_clear_contents(
        buffer: *mut Buffer,
    );
    #[link_name = "hb_buffer_pre_allocate"]
    pub fn buffer_pre_allocate(
        buffer: *mut Buffer,
        size: c_uint,
    ) -> Bool;
    #[link_name = "hb_buffer_allocation_successful"]
    pub fn buffer_allocation_successful(
        buffer: *mut Buffer,
    ) -> Bool;
    #[link_name = "hb_buffer_reverse"]
    pub fn buffer_reverse(
        buffer: *mut Buffer,
    );
    #[link_name = "hb_buffer_reverse_range"]
    pub fn buffer_reverse_range(
        buffer: *mut Buffer,
        start: c_uint,
        end: c_uint,
    );
    #[link_name = "hb_buffer_reverse_clusters"]
    pub fn buffer_reverse_clusters(
        buffer: *mut Buffer,
    );
    #[link_name = "hb_buffer_add"]
    pub fn buffer_add(
        buffer: *mut Buffer,
        codepoint: Codepoint,
        cluster: c_uint,
    );
    #[link_name = "hb_buffer_add_utf8"]
    pub fn buffer_add_utf8(
        buffer: *mut Buffer,
        text: *const c_char,
        text_length: c_int,
        item_offset: c_uint,
        item_length: c_int,
    );
    #[link_name = "hb_buffer_add_utf16"]
    pub fn buffer_add_utf16(
        buffer: *mut Buffer,
        text: *const u16,
        text_length: c_int,
        item_offset: c_uint,
        item_length: c_int,
    );
    #[link_name = "hb_buffer_add_utf32"]
    pub fn buffer_add_utf32(
        buffer: *mut Buffer,
        text: *const u32,
        text_length: c_int,
        item_offset: c_uint,
        item_length: c_int,
    );
    #[link_name = "hb_buffer_add_latin1"]
    pub fn buffer_add_latin1(
        buffer: *mut Buffer,
        text: *const u8,
        text_length: c_int,
        item_offset: c_uint,
        item_length: c_int,
    );
    #[link_name = "hb_buffer_add_codepoints"]
    pub fn buffer_add_codepoints(
        buffer: *mut Buffer,
        text: *const Codepoint,
        text_length: c_int,
        item_offset: c_uint,
        item_length: c_int,
    );
    #[link_name = "hb_buffer_append"]
    pub fn buffer_append(
        buffer: *mut Buffer,
        source: *mut Buffer,
        start: c_uint,
        end: c_uint,
    );
    #[link_name = "hb_buffer_set_length"]
    pub fn buffer_set_length(
        buffer: *mut Buffer,
        length: c_uint,
    ) -> Bool;
    #[link_name = "hb_buffer_get_length"]
    pub fn buffer_get_length(
        buffer: *mut Buffer,
    ) -> c_uint;
    #[link_name = "hb_buffer_get_glyph_infos"]
    pub fn buffer_get_glyph_infos(
        buffer: *mut Buffer,
        length: *mut c_uint,
    ) -> *mut GlyphInfo;
    #[link_name = "hb_buffer_get_glyph_positions"]
    pub fn buffer_get_glyph_positions(
        buffer: *mut Buffer,
        length: *mut c_uint,
    ) -> *mut GlyphPosition;
    #[link_name = "hb_buffer_normalize_glyphs"]
    pub fn buffer_normalize_glyphs(
        buffer: *mut Buffer,
    );
    #[link_name = "hb_buffer_serialize_format_from_string"]
    pub fn buffer_serialize_format_from_string(
        str: *const c_char,
        len: c_int,
    ) -> BufferSerializeFormat;
    #[link_name = "hb_buffer_serialize_format_to_string"]
    pub fn buffer_serialize_format_to_string(
        format: BufferSerializeFormat,
    ) -> *const c_char;
    #[link_name = "hb_buffer_serialize_list_formats"]
    pub fn buffer_serialize_list_formats(
    ) -> *mut *const c_char;
    #[link_name = "hb_buffer_serialize_glyphs"]
    pub fn buffer_serialize_glyphs(
        buffer: *mut Buffer,
        start: c_uint,
        end: c_uint,
        buf: *mut c_char,
        buf_size: c_uint,
        buf_consumed: *mut c_uint,
        font: *mut Font,
        format: BufferSerializeFormat,
        flags: BufferSerializeFlags,
    ) -> c_uint;
    #[link_name = "hb_buffer_deserialize_glyphs"]
    pub fn buffer_deserialize_glyphs(
        buffer: *mut Buffer,
        buf: *const c_char,
        buf_len: c_int,
        end_ptr: *mut *const c_char,
        font: *mut Font,
        format: BufferSerializeFormat,
    ) -> Bool;
    #[link_name = "hb_buffer_diff"]
    pub fn buffer_diff(
        buffer: *mut Buffer,
        reference: *mut Buffer,
        dottedcircle_glyph: Codepoint,
        position_fuzz: c_uint,
    ) -> BufferDiffFlags;
    #[link_name = "hb_buffer_set_message_func"]
    pub fn buffer_set_message_func(
        buffer: *mut Buffer,
        func: BufferMessageFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_glyph_func"]
    pub fn font_funcs_set_glyph_func(
        ffuncs: *mut FontFuncs,
        func: FontGetGlyphFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_set_invert"]
    pub fn set_invert(
        set: *mut Set,
    );
    #[link_name = "hb_unicode_funcs_set_eastasian_width_func"]
    pub fn unicode_funcs_set_eastasian_width_func(
        ufuncs: *mut UnicodeFuncs,
        func: UnicodeEastasianWidthFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_unicode_eastasian_width"]
    pub fn unicode_eastasian_width(
        ufuncs: *mut UnicodeFuncs,
        unicode: Codepoint,
    ) -> c_uint;
    #[link_name = "hb_unicode_funcs_set_decompose_compatibility_func"]
    pub fn unicode_funcs_set_decompose_compatibility_func(
        ufuncs: *mut UnicodeFuncs,
        func: UnicodeDecomposeCompatibilityFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_unicode_decompose_compatibility"]
    pub fn unicode_decompose_compatibility(
        ufuncs: *mut UnicodeFuncs,
        u: Codepoint,
        decomposed: *mut Codepoint,
    ) -> c_uint;
    #[link_name = "hb_font_funcs_set_glyph_h_kerning_func"]
    pub fn font_funcs_set_glyph_h_kerning_func(
        ffuncs: *mut FontFuncs,
        func: FontGetGlyphHKerningFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_funcs_set_glyph_v_kerning_func"]
    pub fn font_funcs_set_glyph_v_kerning_func(
        ffuncs: *mut FontFuncs,
        func: FontGetGlyphVKerningFunc,
        user_data: *mut c_void,
        destroy: DestroyFunc,
    );
    #[link_name = "hb_font_get_glyph_h_kerning"]
    pub fn font_get_glyph_h_kerning(
        font: *mut Font,
        left_glyph: Codepoint,
        right_glyph: Codepoint,
    ) -> Position;
    #[link_name = "hb_font_get_glyph_v_kerning"]
    pub fn font_get_glyph_v_kerning(
        font: *mut Font,
        top_glyph: Codepoint,
        bottom_glyph: Codepoint,
    ) -> Position;
    #[link_name = "hb_font_get_glyph_kerning_for_direction"]
    pub fn font_get_glyph_kerning_for_direction(
        font: *mut Font,
        first_glyph: Codepoint,
        second_glyph: Codepoint,
        direction: Direction,
        x: *mut Position,
        y: *mut Position,
    );
    #[link_name = "hb_map_create"]
    pub fn map_create(
    ) -> *mut Map;
    #[link_name = "hb_map_get_empty"]
    pub fn map_get_empty(
    ) -> *mut Map;
    #[link_name = "hb_map_reference"]
    pub fn map_reference(
        map: *mut Map,
    ) -> *mut Map;
    #[link_name = "hb_map_destroy"]
    pub fn map_destroy(
        map: *mut Map,
    );
    #[link_name = "hb_map_set_user_data"]
    pub fn map_set_user_data(
        map: *mut Map,
        key: *mut UserDataKey,
        data: *mut c_void,
        destroy: DestroyFunc,
        replace: Bool,
    ) -> Bool;
    #[link_name = "hb_map_get_user_data"]
    pub fn map_get_user_data(
        map: *mut Map,
        key: *mut UserDataKey,
    ) -> *mut c_void;
    #[link_name = "hb_map_allocation_successful"]
    pub fn map_allocation_successful(
        map: *const Map,
    ) -> Bool;
    #[link_name = "hb_map_clear"]
    pub fn map_clear(
        map: *mut Map,
    );
    #[link_name = "hb_map_is_empty"]
    pub fn map_is_empty(
        map: *const Map,
    ) -> Bool;
    #[link_name = "hb_map_get_population"]
    pub fn map_get_population(
        map: *const Map,
    ) -> c_uint;
    #[link_name = "hb_map_set"]
    pub fn map_set(
        map: *mut Map,
        key: Codepoint,
        value: Codepoint,
    );
    #[link_name = "hb_map_get"]
    pub fn map_get(
        map: *const Map,
        key: Codepoint,
    ) -> Codepoint;
    #[link_name = "hb_map_del"]
    pub fn map_del(
        map: *mut Map,
        key: Codepoint,
    );
    #[link_name = "hb_map_has"]
    pub fn map_has(
        map: *const Map,
        key: Codepoint,
    ) -> Bool;
    #[link_name = "hb_shape"]
    pub fn shape(
        font: *mut Font,
        buffer: *mut Buffer,
        features: *const Feature,
        num_features: c_uint,
    );
    #[link_name = "hb_shape_full"]
    pub fn shape_full(
        font: *mut Font,
        buffer: *mut Buffer,
        features: *const Feature,
        num_features: c_uint,
        shaper_list: *const *const c_char,
    ) -> Bool;
    #[link_name = "hb_shape_list_shapers"]
    pub fn shape_list_shapers(
    ) -> *mut *const c_char;
    #[link_name = "hb_shape_plan_create"]
    pub fn shape_plan_create(
        face: *mut Face,
        props: *const SegmentProperties,
        user_features: *const Feature,
        num_user_features: c_uint,
        shaper_list: *const *const c_char,
    ) -> *mut ShapePlan;
    #[link_name = "hb_shape_plan_create_cached"]
    pub fn shape_plan_create_cached(
        face: *mut Face,
        props: *const SegmentProperties,
        user_features: *const Feature,
        num_user_features: c_uint,
        shaper_list: *const *const c_char,
    ) -> *mut ShapePlan;
    #[link_name = "hb_shape_plan_create2"]
    pub fn shape_plan_create2(
        face: *mut Face,
        props: *const SegmentProperties,
        user_features: *const Feature,
        num_user_features: c_uint,
        coords: *const c_int,
        num_coords: c_uint,
        shaper_list: *const *const c_char,
    ) -> *mut ShapePlan;
    #[link_name = "hb_shape_plan_create_cached2"]
    pub fn shape_plan_create_cached2(
        face: *mut Face,
        props: *const SegmentProperties,
        user_features: *const Feature,
        num_user_features: c_uint,
        coords: *const c_int,
        num_coords: c_uint,
        shaper_list: *const *const c_char,
    ) -> *mut ShapePlan;
    #[link_name = "hb_shape_plan_get_empty"]
    pub fn shape_plan_get_empty(
    ) -> *mut ShapePlan;
    #[link_name = "hb_shape_plan_reference"]
    pub fn shape_plan_reference(
        shape_plan: *mut ShapePlan,
    ) -> *mut ShapePlan;
    #[link_name = "hb_shape_plan_destroy"]
    pub fn shape_plan_destroy(
        shape_plan: *mut ShapePlan,
    );
    #[link_name = "hb_shape_plan_set_user_data"]
    pub fn shape_plan_set_user_data(
        shape_plan: *mut ShapePlan,
        key: *mut UserDataKey,
        data: *mut c_void,
        destroy: DestroyFunc,
        replace: Bool,
    ) -> Bool;
    #[link_name = "hb_shape_plan_get_user_data"]
    pub fn shape_plan_get_user_data(
        shape_plan: *mut ShapePlan,
        key: *mut UserDataKey,
    ) -> *mut c_void;
    #[link_name = "hb_shape_plan_execute"]
    pub fn shape_plan_execute(
        shape_plan: *mut ShapePlan,
        font: *mut Font,
        buffer: *mut Buffer,
        features: *const Feature,
        num_features: c_uint,
    ) -> Bool;
    #[link_name = "hb_shape_plan_get_shaper"]
    pub fn shape_plan_get_shaper(
        shape_plan: *mut ShapePlan,
    ) -> *const c_char;
    #[link_name = "hb_version"]
    pub fn version(
        major: *mut c_uint,
        minor: *mut c_uint,
        micro: *mut c_uint,
    );
    #[link_name = "hb_version_string"]
    pub fn version_string(
    ) -> *const c_char;
    #[link_name = "hb_version_atleast"]
    pub fn version_atleast(
        major: c_uint,
        minor: c_uint,
        micro: c_uint,
    ) -> Bool;
}

#[cfg(feature = "freetype")]
extern crate freetype_ffi;
#[cfg(feature = "freetype")]
pub mod ft;

pub mod ot;
