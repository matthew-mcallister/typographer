use std::os::raw::*;

use freetype_ffi::FT_Face;

use crate::*;

extern "C" {
    #[link_name = "hb_ft_face_create"]
    pub fn face_create(
        ft_face: FT_Face,
        destroy: DestroyFunc,
    ) -> *mut Face;
    #[link_name = "hb_ft_face_create_cached"]
    pub fn face_create_cached(
        ft_face: FT_Face,
    ) -> *mut Face;
    #[link_name = "hb_ft_face_create_referenced"]
    pub fn face_create_referenced(
        ft_face: FT_Face,
    ) -> *mut Face;
    #[link_name = "hb_ft_font_create"]
    pub fn font_create(
        ft_face: FT_Face,
        destroy: DestroyFunc,
    ) -> *mut Font;
    #[link_name = "hb_ft_font_create_referenced"]
    pub fn font_create_referenced(
        ft_face: FT_Face,
    ) -> *mut Font;
    #[link_name = "hb_ft_font_get_face"]
    pub fn font_get_face(
        font: *mut Font,
    ) -> FT_Face;
    #[link_name = "hb_ft_font_set_load_flags"]
    pub fn font_set_load_flags(
        font: *mut Font,
        load_flags: c_int,
    );
    #[link_name = "hb_ft_font_get_load_flags"]
    pub fn font_get_load_flags(
        font: *mut Font,
    ) -> c_int;
    #[link_name = "hb_ft_font_changed"]
    pub fn font_changed(
        font: *mut Font,
    );
    #[link_name = "hb_ft_font_set_funcs"]
    pub fn font_set_funcs(
        font: *mut Font,
    );
}
