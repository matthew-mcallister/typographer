use crate::*;

pub type NameId = c_uint;

impl_struct! {
    NameEntry;
    name_id: NameId,
    var: VarInt,
    language: Language,
}
impl_struct! {
    VarAxis;
    tag: Tag,
    name_id: NameId,
    min_value: f32,
    default_value: f32,
    max_value: f32,
}
impl_struct! {
    ColorLayer;
    glyph: Codepoint,
    color_index: c_uint,
}
impl_struct! {
    MathGlyphVariant;
    glyph: Codepoint,
    advance: Position,
}
impl_struct! {
    MathGlyphPart;
    glyph: Codepoint,
    start_connector_length: Position,
    end_connector_length: Position,
    full_advance: Position,
    flags: MathGlyphPartFlags,
}
impl_struct! {
    VarAxisInfo;
    axis_index: c_uint,
    tag: Tag,
    name_id: NameId,
    flags: VarAxisFlags,
    min_value: f32,
    default_value: f32,
    max_value: f32,
    reserved: c_uint,
}

impl_enum! {
    LayoutGlyphClass;
    UNCLASSIFIED = 0;
    BASE_GLYPH = 1;
    LIGATURE = 2;
    MARK = 3;
    COMPONENT = 4;
}
impl_enum! {
    MathConstant;
    SCRIPT_PERCENT_SCALE_DOWN = 0;
    SCRIPT_SCRIPT_PERCENT_SCALE_DOWN = 1;
    DELIMITED_SUB_FORMULA_MIN_HEIGHT = 2;
    DISPLAY_OPERATOR_MIN_HEIGHT = 3;
    MATH_LEADING = 4;
    AXIS_HEIGHT = 5;
    ACCENT_BASE_HEIGHT = 6;
    FLATTENED_ACCENT_BASE_HEIGHT = 7;
    SUBSCRIPT_SHIFT_DOWN = 8;
    SUBSCRIPT_TOP_MAX = 9;
    SUBSCRIPT_BASELINE_DROP_MIN = 10;
    SUPERSCRIPT_SHIFT_UP = 11;
    SUPERSCRIPT_SHIFT_UP_CRAMPED = 12;
    SUPERSCRIPT_BOTTOM_MIN = 13;
    SUPERSCRIPT_BASELINE_DROP_MAX = 14;
    SUB_SUPERSCRIPT_GAP_MIN = 15;
    SUPERSCRIPT_BOTTOM_MAX_WITH_SUBSCRIPT = 16;
    SPACE_AFTER_SCRIPT = 17;
    UPPER_LIMIT_GAP_MIN = 18;
    UPPER_LIMIT_BASELINE_RISE_MIN = 19;
    LOWER_LIMIT_GAP_MIN = 20;
    LOWER_LIMIT_BASELINE_DROP_MIN = 21;
    STACK_TOP_SHIFT_UP = 22;
    STACK_TOP_DISPLAY_STYLE_SHIFT_UP = 23;
    STACK_BOTTOM_SHIFT_DOWN = 24;
    STACK_BOTTOM_DISPLAY_STYLE_SHIFT_DOWN = 25;
    STACK_GAP_MIN = 26;
    STACK_DISPLAY_STYLE_GAP_MIN = 27;
    STRETCH_STACK_TOP_SHIFT_UP = 28;
    STRETCH_STACK_BOTTOM_SHIFT_DOWN = 29;
    STRETCH_STACK_GAP_ABOVE_MIN = 30;
    STRETCH_STACK_GAP_BELOW_MIN = 31;
    FRACTION_NUMERATOR_SHIFT_UP = 32;
    FRACTION_NUMERATOR_DISPLAY_STYLE_SHIFT_UP = 33;
    FRACTION_DENOMINATOR_SHIFT_DOWN = 34;
    FRACTION_DENOMINATOR_DISPLAY_STYLE_SHIFT_DOWN = 35;
    FRACTION_NUMERATOR_GAP_MIN = 36;
    FRACTION_NUM_DISPLAY_STYLE_GAP_MIN = 37;
    FRACTION_RULE_THICKNESS = 38;
    FRACTION_DENOMINATOR_GAP_MIN = 39;
    FRACTION_DENOM_DISPLAY_STYLE_GAP_MIN = 40;
    SKEWED_FRACTION_HORIZONTAL_GAP = 41;
    SKEWED_FRACTION_VERTICAL_GAP = 42;
    OVERBAR_VERTICAL_GAP = 43;
    OVERBAR_RULE_THICKNESS = 44;
    OVERBAR_EXTRA_ASCENDER = 45;
    UNDERBAR_VERTICAL_GAP = 46;
    UNDERBAR_RULE_THICKNESS = 47;
    UNDERBAR_EXTRA_DESCENDER = 48;
    RADICAL_VERTICAL_GAP = 49;
    RADICAL_DISPLAY_STYLE_VERTICAL_GAP = 50;
    RADICAL_RULE_THICKNESS = 51;
    RADICAL_EXTRA_ASCENDER = 52;
    RADICAL_KERN_BEFORE_DEGREE = 53;
    RADICAL_KERN_AFTER_DEGREE = 54;
    RADICAL_DEGREE_BOTTOM_RAISE_PERCENT = 55;
}
impl_enum! {
    MathKern;
    TOP_RIGHT = 0;
    TOP_LEFT = 1;
    BOTTOM_RIGHT = 2;
    BOTTOM_LEFT = 3;
}

impl_flags! {
    ColorPaletteFlags;
    DEFAULT = 0;
    USABLE_WITH_LIGHT_BACKGROUND = 1;
    USABLE_WITH_DARK_BACKGROUND = 2;
}
impl_flags! {
    MathGlyphPartFlags;
    EXTENDER = 1;
}
impl_flags! {
    VarAxisFlags;
    HIDDEN = 1;
}

extern "C" {
    #[link_name = "hb_ot_name_list_names"]
    pub fn name_list_names(
        face: *mut Face,
        num_entries: *mut c_uint,
    ) -> *const NameEntry;
    #[link_name = "hb_ot_name_get_utf8"]
    pub fn name_get_utf8(
        face: *mut Face,
        name_id: NameId,
        language: Language,
        text_size: *mut c_uint,
        text: *mut c_char,
    ) -> c_uint;
    #[link_name = "hb_ot_name_get_utf16"]
    pub fn name_get_utf16(
        face: *mut Face,
        name_id: NameId,
        language: Language,
        text_size: *mut c_uint,
        text: *mut u16,
    ) -> c_uint;
    #[link_name = "hb_ot_name_get_utf32"]
    pub fn name_get_utf32(
        face: *mut Face,
        name_id: NameId,
        language: Language,
        text_size: *mut c_uint,
        text: *mut u32,
    ) -> c_uint;
    #[link_name = "hb_ot_color_has_palettes"]
    pub fn color_has_palettes(
        face: *mut Face,
    ) -> Bool;
    #[link_name = "hb_ot_color_palette_get_count"]
    pub fn color_palette_get_count(
        face: *mut Face,
    ) -> c_uint;
    #[link_name = "hb_ot_color_palette_get_name_id"]
    pub fn color_palette_get_name_id(
        face: *mut Face,
        palette_index: c_uint,
    ) -> NameId;
    #[link_name = "hb_ot_color_palette_color_get_name_id"]
    pub fn color_palette_color_get_name_id(
        face: *mut Face,
        color_index: c_uint,
    ) -> NameId;
    #[link_name = "hb_ot_color_palette_get_flags"]
    pub fn color_palette_get_flags(
        face: *mut Face,
        palette_index: c_uint,
    ) -> ColorPaletteFlags;
    #[link_name = "hb_ot_color_palette_get_colors"]
    pub fn color_palette_get_colors(
        face: *mut Face,
        palette_index: c_uint,
        start_offset: c_uint,
        color_count: *mut c_uint,
        colors: *mut Color,
    ) -> c_uint;
    #[link_name = "hb_ot_color_has_layers"]
    pub fn color_has_layers(
        face: *mut Face,
    ) -> Bool;
    #[link_name = "hb_ot_color_glyph_get_layers"]
    pub fn color_glyph_get_layers(
        face: *mut Face,
        glyph: Codepoint,
        start_offset: c_uint,
        count: *mut c_uint,
        layers: *mut ColorLayer,
    ) -> c_uint;
    #[link_name = "hb_ot_color_has_svg"]
    pub fn color_has_svg(
        face: *mut Face,
    ) -> Bool;
    #[link_name = "hb_ot_color_glyph_reference_svg"]
    pub fn color_glyph_reference_svg(
        face: *mut Face,
        glyph: Codepoint,
    ) -> *mut Blob;
    #[link_name = "hb_ot_color_has_png"]
    pub fn color_has_png(
        face: *mut Face,
    ) -> Bool;
    #[link_name = "hb_ot_color_glyph_reference_png"]
    pub fn color_glyph_reference_png(
        font: *mut Font,
        glyph: Codepoint,
    ) -> *mut Blob;
    #[link_name = "LayoutTableChooseScri"]
    pub fn layout_table_choose_script(
        face: *mut Face,
        table_tag: Tag,
        script_tags: *const Tag,
        script_index: *mut c_uint,
        chosen_script: *mut Tag,
    ) -> Bool;
    #[link_name = "hb_ot_layout_script_find_language"]
    pub fn layout_script_find_language(
        face: *mut Face,
        table_tag: Tag,
        script_index: c_uint,
        language_tag: Tag,
        language_index: *mut c_uint,
    ) -> Bool;
    #[link_name = "OtTagsFromScri"]
    pub fn tags_from_script(
        script: Script,
        script_tag_1: *mut Tag,
        script_tag_2: *mut Tag,
    );
    #[link_name = "OtTagFromLangua"]
    pub fn tag_from_language(
        language: Language,
    ) -> Tag;
    #[link_name = "hb_ot_var_get_axes"]
    pub fn var_get_axes(
        face: *mut Face,
        start_offset: c_uint,
        axes_count: *mut c_uint,
        axes_array: *mut VarAxis,
    ) -> c_uint;
    #[link_name = "hb_ot_var_find_axis"]
    pub fn var_find_axis(
        face: *mut Face,
        axis_tag: Tag,
        axis_index: *mut c_uint,
        axis_info: *mut VarAxis,
    ) -> Bool;
    #[link_name = "hb_ot_font_set_funcs"]
    pub fn font_set_funcs(
        font: *mut Font,
    );
    #[link_name = "OtTagsFromScriptAndLangua"]
    pub fn tags_from_script_and_language(
        script: Script,
        language: Language,
        script_count: *mut c_uint,
        script_tags: *mut Tag,
        language_count: *mut c_uint,
        language_tags: *mut Tag,
    );
    #[link_name = "TagToScri"]
    pub fn tag_to_script(
        tag: Tag,
    ) -> Script;
    #[link_name = "TagToLangua"]
    pub fn tag_to_language(
        tag: Tag,
    ) -> Language;
    #[link_name = "TagsToScriptAndLangua"]
    pub fn tags_to_script_and_language(
        script_tag: Tag,
        language_tag: Tag,
        script: *mut Script,
        language: *mut Language,
    );
    #[link_name = "hb_ot_layout_has_glyph_classes"]
    pub fn layout_has_glyph_classes(
        face: *mut Face,
    ) -> Bool;
    #[link_name = "hb_ot_layout_get_glyph_class"]
    pub fn layout_get_glyph_class(
        face: *mut Face,
        glyph: Codepoint,
    ) -> LayoutGlyphClass;
    #[link_name = "hb_ot_layout_get_glyphs_in_class"]
    pub fn layout_get_glyphs_in_class(
        face: *mut Face,
        klass: LayoutGlyphClass,
        glyphs: *mut Set,
    );
    #[link_name = "hb_ot_layout_get_attach_points"]
    pub fn layout_get_attach_points(
        face: *mut Face,
        glyph: Codepoint,
        start_offset: c_uint,
        point_count: *mut c_uint,
        point_array: *mut c_uint,
    ) -> c_uint;
    #[link_name = "hb_ot_layout_get_ligature_carets"]
    pub fn layout_get_ligature_carets(
        font: *mut Font,
        direction: Direction,
        glyph: Codepoint,
        start_offset: c_uint,
        caret_count: *mut c_uint,
        caret_array: *mut Position,
    ) -> c_uint;
    #[link_name = "LayoutTableGetScriptTa"]
    pub fn layout_table_get_script_tags(
        face: *mut Face,
        table_tag: Tag,
        start_offset: c_uint,
        script_count: *mut c_uint,
        script_tags: *mut Tag,
    ) -> c_uint;
    #[link_name = "LayoutTableFindScri"]
    pub fn layout_table_find_script(
        face: *mut Face,
        table_tag: Tag,
        script_tag: Tag,
        script_index: *mut c_uint,
    ) -> Bool;
    #[link_name = "LayoutTableSelectScri"]
    pub fn layout_table_select_script(
        face: *mut Face,
        table_tag: Tag,
        script_count: c_uint,
        script_tags: *const Tag,
        script_index: *mut c_uint,
        chosen_script: *mut Tag,
    ) -> Bool;
    #[link_name = "LayoutTableGetFeatureTa"]
    pub fn layout_table_get_feature_tags(
        face: *mut Face,
        table_tag: Tag,
        start_offset: c_uint,
        feature_count: *mut c_uint,
        feature_tags: *mut Tag,
    ) -> c_uint;
    #[link_name = "LayoutScriptGetLanguageTa"]
    pub fn layout_script_get_language_tags(
        face: *mut Face,
        table_tag: Tag,
        script_index: c_uint,
        start_offset: c_uint,
        language_count: *mut c_uint,
        language_tags: *mut Tag,
    ) -> c_uint;
    #[link_name = "hb_ot_layout_script_select_language"]
    pub fn layout_script_select_language(
        face: *mut Face,
        table_tag: Tag,
        script_index: c_uint,
        language_count: c_uint,
        language_tags: *const Tag,
        language_index: *mut c_uint,
    ) -> Bool;
    #[link_name = "hb_ot_layout_language_get_required_feature_index"]
    pub fn layout_language_get_required_feature_index(
        face: *mut Face,
        table_tag: Tag,
        script_index: c_uint,
        language_index: c_uint,
        feature_index: *mut c_uint,
    ) -> Bool;
    #[link_name = "hb_ot_layout_language_get_required_feature"]
    pub fn layout_language_get_required_feature(
        face: *mut Face,
        table_tag: Tag,
        script_index: c_uint,
        language_index: c_uint,
        feature_index: *mut c_uint,
        feature_tag: *mut Tag,
    ) -> Bool;
    #[link_name = "hb_ot_layout_language_get_feature_indexes"]
    pub fn layout_language_get_feature_indexes(
        face: *mut Face,
        table_tag: Tag,
        script_index: c_uint,
        language_index: c_uint,
        start_offset: c_uint,
        feature_count: *mut c_uint,
        feature_indexes: *mut c_uint,
    ) -> c_uint;
    #[link_name = "LayoutLanguageGetFeatureTa"]
    pub fn layout_language_get_feature_tags(
        face: *mut Face,
        table_tag: Tag,
        script_index: c_uint,
        language_index: c_uint,
        start_offset: c_uint,
        feature_count: *mut c_uint,
        feature_tags: *mut Tag,
    ) -> c_uint;
    #[link_name = "hb_ot_layout_language_find_feature"]
    pub fn layout_language_find_feature(
        face: *mut Face,
        table_tag: Tag,
        script_index: c_uint,
        language_index: c_uint,
        feature_tag: Tag,
        feature_index: *mut c_uint,
    ) -> Bool;
    #[link_name = "hb_ot_layout_feature_get_lookups"]
    pub fn layout_feature_get_lookups(
        face: *mut Face,
        table_tag: Tag,
        feature_index: c_uint,
        start_offset: c_uint,
        lookup_count: *mut c_uint,
        lookup_indexes: *mut c_uint,
    ) -> c_uint;
    #[link_name = "LayoutTableGetLookupCou"]
    pub fn layout_table_get_lookup_count(
        face: *mut Face,
        table_tag: Tag,
    ) -> c_uint;
    #[link_name = "hb_ot_layout_collect_features"]
    pub fn layout_collect_features(
        face: *mut Face,
        table_tag: Tag,
        scripts: *const Tag,
        languages: *const Tag,
        features: *const Tag,
        feature_indexes: *mut Set,
    );
    #[link_name = "hb_ot_layout_collect_lookups"]
    pub fn layout_collect_lookups(
        face: *mut Face,
        table_tag: Tag,
        scripts: *const Tag,
        languages: *const Tag,
        features: *const Tag,
        lookup_indexes: *mut Set,
    );
    #[link_name = "hb_ot_layout_lookup_collect_glyphs"]
    pub fn layout_lookup_collect_glyphs(
        face: *mut Face,
        table_tag: Tag,
        lookup_index: c_uint,
        glyphs_before: *mut Set,
        glyphs_input: *mut Set,
        glyphs_after: *mut Set,
        glyphs_output: *mut Set,
    );
    #[link_name = "LayoutTableFindFeatureVariatio"]
    pub fn layout_table_find_feature_variations(
        face: *mut Face,
        table_tag: Tag,
        coords: *const c_int,
        num_coords: c_uint,
        variations_index: *mut c_uint,
    ) -> Bool;
    #[link_name = "hb_ot_layout_feature_with_variations_get_lookups"]
    pub fn layout_feature_with_variations_get_lookups(
        face: *mut Face,
        table_tag: Tag,
        feature_index: c_uint,
        variations_index: c_uint,
        start_offset: c_uint,
        lookup_count: *mut c_uint,
        lookup_indexes: *mut c_uint,
    ) -> c_uint;
    #[link_name = "hb_ot_layout_has_substitution"]
    pub fn layout_has_substitution(
        face: *mut Face,
    ) -> Bool;
    #[link_name = "hb_ot_layout_lookup_would_substitute"]
    pub fn layout_lookup_would_substitute(
        face: *mut Face,
        lookup_index: c_uint,
        glyphs: *const Codepoint,
        glyphs_length: c_uint,
        zero_context: Bool,
    ) -> Bool;
    #[link_name = "hb_ot_layout_lookup_substitute_closure"]
    pub fn layout_lookup_substitute_closure(
        face: *mut Face,
        lookup_index: c_uint,
        glyphs: *mut Set,
    );
    #[link_name = "hb_ot_layout_lookups_substitute_closure"]
    pub fn layout_lookups_substitute_closure(
        face: *mut Face,
        lookups: *const Set,
        glyphs: *mut Set,
    );
    #[link_name = "hb_ot_layout_has_positioning"]
    pub fn layout_has_positioning(
        face: *mut Face,
    ) -> Bool;
    #[link_name = "hb_ot_layout_get_size_params"]
    pub fn layout_get_size_params(
        face: *mut Face,
        design_size: *mut c_uint,
        subfamily_id: *mut c_uint,
        subfamily_name_id: *mut NameId,
        range_start: *mut c_uint,
        range_end: *mut c_uint,
    ) -> Bool;
    #[link_name = "hb_ot_layout_feature_get_name_ids"]
    pub fn layout_feature_get_name_ids(
        face: *mut Face,
        table_tag: Tag,
        feature_index: c_uint,
        label_id: *mut NameId,
        tooltip_id: *mut NameId,
        sample_id: *mut NameId,
        num_named_parameters: *mut c_uint,
        first_param_id: *mut NameId,
    ) -> Bool;
    #[link_name = "hb_ot_layout_feature_get_characters"]
    pub fn layout_feature_get_characters(
        face: *mut Face,
        table_tag: Tag,
        feature_index: c_uint,
        start_offset: c_uint,
        char_count: *mut c_uint,
        characters: *mut Codepoint,
    ) -> c_uint;
    #[link_name = "hb_ot_math_has_data"]
    pub fn math_has_data(
        face: *mut Face,
    ) -> Bool;
    #[link_name = "hb_ot_math_get_constant"]
    pub fn math_get_constant(
        font: *mut Font,
        constant: MathConstant,
    ) -> Position;
    #[link_name = "hb_ot_math_get_glyph_italics_correction"]
    pub fn math_get_glyph_italics_correction(
        font: *mut Font,
        glyph: Codepoint,
    ) -> Position;
    #[link_name = "MathGetGlyphTopAccentAttachme"]
    pub fn math_get_glyph_top_accent_attachment(
        font: *mut Font,
        glyph: Codepoint,
    ) -> Position;
    #[link_name = "hb_ot_math_is_glyph_extended_shape"]
    pub fn math_is_glyph_extended_shape(
        face: *mut Face,
        glyph: Codepoint,
    ) -> Bool;
    #[link_name = "hb_ot_math_get_glyph_kerning"]
    pub fn math_get_glyph_kerning(
        font: *mut Font,
        glyph: Codepoint,
        kern: MathKern,
        correction_height: Position,
    ) -> Position;
    #[link_name = "hb_ot_math_get_glyph_variants"]
    pub fn math_get_glyph_variants(
        font: *mut Font,
        glyph: Codepoint,
        direction: Direction,
        start_offset: c_uint,
        variants_count: *mut c_uint,
        variants: *mut MathGlyphVariant,
    ) -> c_uint;
    #[link_name = "hb_ot_math_get_min_connector_overlap"]
    pub fn math_get_min_connector_overlap(
        font: *mut Font,
        direction: Direction,
    ) -> Position;
    #[link_name = "hb_ot_math_get_glyph_assembly"]
    pub fn math_get_glyph_assembly(
        font: *mut Font,
        glyph: Codepoint,
        direction: Direction,
        start_offset: c_uint,
        parts_count: *mut c_uint,
        parts: *mut MathGlyphPart,
        italics_correction: *mut Position,
    ) -> c_uint;
    #[link_name = "hb_ot_shape_glyphs_closure"]
    pub fn shape_glyphs_closure(
        font: *mut Font,
        buffer: *mut Buffer,
        features: *const Feature,
        num_features: c_uint,
        glyphs: *mut Set,
    );
    #[link_name = "hb_ot_shape_plan_collect_lookups"]
    pub fn shape_plan_collect_lookups(
        shape_plan: *mut ShapePlan,
        table_tag: Tag,
        lookup_indexes: *mut Set,
    );
    #[link_name = "hb_ot_var_has_data"]
    pub fn var_has_data(
        face: *mut Face,
    ) -> Bool;
    #[link_name = "hb_ot_var_get_axis_count"]
    pub fn var_get_axis_count(
        face: *mut Face,
    ) -> c_uint;
    #[link_name = "hb_ot_var_get_axis_infos"]
    pub fn var_get_axis_infos(
        face: *mut Face,
        start_offset: c_uint,
        axes_count: *mut c_uint,
        axes_array: *mut VarAxisInfo,
    ) -> c_uint;
    #[link_name = "hb_ot_var_find_axis_info"]
    pub fn var_find_axis_info(
        face: *mut Face,
        axis_tag: Tag,
        axis_info: *mut VarAxisInfo,
    ) -> Bool;
    #[link_name = "hb_ot_var_get_named_instance_count"]
    pub fn var_get_named_instance_count(
        face: *mut Face,
    ) -> c_uint;
    #[link_name = "hb_ot_var_named_instance_get_subfamily_name_id"]
    pub fn var_named_instance_get_subfamily_name_id(
        face: *mut Face,
        instance_index: c_uint,
    ) -> NameId;
    #[link_name = "hb_ot_var_named_instance_get_postscript_name_id"]
    pub fn var_named_instance_get_postscript_name_id(
        face: *mut Face,
        instance_index: c_uint,
    ) -> NameId;
    #[link_name = "hb_ot_var_named_instance_get_design_coords"]
    pub fn var_named_instance_get_design_coords(
        face: *mut Face,
        instance_index: c_uint,
        coords_length: *mut c_uint,
        coords: *mut f32,
    ) -> c_uint;
    #[link_name = "hb_ot_var_normalize_variations"]
    pub fn var_normalize_variations(
        face: *mut Face,
        variations: *const Variation,
        variations_length: c_uint,
        coords: *mut c_int,
        coords_length: c_uint,
    );
    #[link_name = "hb_ot_var_normalize_coords"]
    pub fn var_normalize_coords(
        face: *mut Face,
        coords_length: c_uint,
        design_coords: *const f32,
        normalized_coords: *mut c_int,
    );
}
